-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 27 mars 2020 à 10:24
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sagge-notes-native`
--

-- --------------------------------------------------------

--
-- Structure de la table `anonymat`
--

CREATE TABLE `anonymat` (
  `id_anonymat` int(11) NOT NULL,
  `code_anonymat` varchar(40) DEFAULT NULL,
  `matricule` varchar(10) NOT NULL,
  `id_classe` int(10) NOT NULL,
  `annee_academique` varchar(10) NOT NULL,
  `code_ue` varchar(10) NOT NULL,
  `session` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `anonymat`
--

INSERT INTO `anonymat` (`id_anonymat`, `code_anonymat`, `matricule`, `id_classe`, `annee_academique`, `code_ue`, `session`) VALUES
(1, 'INF411 - 001', '18B110EB', 8, '2018-2019', 'INF411', 'session_no'),
(2, 'INF411 - 002', '18B103EB', 8, '2018-2019', 'INF411', 'session_no'),
(3, 'INF411 - 003', '18B119EB', 8, '2018-2019', 'INF411', 'session_no'),
(4, 'INF411 - 004', '18B104EB', 8, '2018-2019', 'INF411', 'session_no'),
(5, 'INF411 - 005', '18B116EB', 8, '2018-2019', 'INF411', 'session_no'),
(6, 'INF411 - 006', '18B106EB', 8, '2018-2019', 'INF411', 'session_no'),
(7, 'INFO1INF411-005', '18A096EB', 1, '2018-2019', 'INF411', 'session_no'),
(8, 'INFO1INF411-006', '18A099EB', 1, '2018-2019', 'INF411', 'session_no'),
(9, 'INFO1SED 452-015', '17A065EB', 2, '2018-2019', 'SED 452', 'session_no'),
(10, 'INFO1SED 452-016', '17A179EB', 2, '2018-2019', 'SED 452', 'session_no'),
(11, 'INFO1SED 452-017', '17A066EB', 2, '2018-2019', 'SED 452', 'session_no'),
(12, 'INFO1SED 452-018', '17A180EB', 2, '2018-2019', 'SED 452', 'session_no'),
(13, 'INFO1SED 452-019', '17A067EB', 2, '2018-2019', 'SED 452', 'session_no'),
(14, 'INFO1SED 452-020', '17A181EB', 2, '2018-2019', 'SED 452', 'session_no'),
(15, 'INFO1SED 452-021', '17A068EB', 2, '2018-2019', 'SED 452', 'session_no'),
(16, 'INFO1SED 452-022', '17A069EB', 2, '2018-2019', 'SED 452', 'session_no'),
(17, 'INFO1SED 452-023', '17A070EB', 2, '2018-2019', 'SED 452', 'session_no'),
(18, 'INFO1SED 452-024', '17A071EB', 2, '2018-2019', 'SED 452', 'session_no'),
(19, 'INFO1SED 452-025', '17A072EB', 2, '2018-2019', 'SED 452', 'session_no'),
(20, 'INFO1SED 452-026', '17A073EB', 2, '2018-2019', 'SED 452', 'session_no'),
(21, 'INFO1SED 452-027', '17A183EB', 2, '2018-2019', 'SED 452', 'session_no'),
(22, 'INFO1SED 452-028', '17A074EB', 2, '2018-2019', 'SED 452', 'session_no'),
(23, 'QFON441-001', '18B110EB', 8, '2018-2019', 'FON441', 'session_no'),
(24, 'QFON441-002', '18B103EB', 8, '2018-2019', 'FON441', 'session_no'),
(25, 'QFON441-003', '18B119EB', 8, '2018-2019', 'FON441', 'session_no'),
(26, 'QFON441-004', '18B104EB', 8, '2018-2019', 'FON441', 'session_no'),
(27, 'QFON441-005', '18B116EB', 8, '2018-2019', 'FON441', 'session_no'),
(28, 'QFON441-006', '18B106EB', 8, '2018-2019', 'FON441', 'session_no'),
(29, 'FONDA016', '17B062EB', 4, '2018-2019', 'FON441', 'session_no'),
(30, 'FONDA017', '18B100EB', 4, '2018-2019', 'FON441', 'session_no'),
(31, 'FONDA018', '18B103EB', 4, '2018-2019', 'FON441', 'session_no'),
(32, 'FONDA019', '18B104EB', 4, '2018-2019', 'FON441', 'session_no'),
(33, 'FONDA020', '18B105EB', 4, '2018-2019', 'FON441', 'session_no'),
(34, 'FONDA021', '18B106EB', 4, '2018-2019', 'FON441', 'session_no'),
(35, 'FON421001', '17B062EB', 4, '2018-2019', 'INF421', 'session_no'),
(36, 'FON421002', '18B100EB', 4, '2018-2019', 'INF421', 'session_no'),
(37, 'FON421003', '18B103EB', 4, '2018-2019', 'INF421', 'session_no'),
(38, 'FON421004', '18B104EB', 4, '2018-2019', 'INF421', 'session_no'),
(39, 'FON421005', '18B105EB', 4, '2018-2019', 'INF421', 'session_no'),
(40, 'FON421006', '18B106EB', 4, '2018-2019', 'INF421', 'session_no');

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `id_classe` int(11) NOT NULL,
  `id_filiere` int(11) DEFAULT NULL,
  `nom_classe` varchar(255) DEFAULT NULL,
  `niveau` int(11) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`id_classe`, `id_filiere`, `nom_classe`, `niveau`, `type`) VALUES
(1, 3, 'Informatique 1', 1, 'physique'),
(2, 3, 'Informatique 2', 2, 'physique'),
(3, 3, 'Informatique 3', 3, 'physique'),
(4, 1, 'Informatique Fondamentale 4', 4, 'physique'),
(5, 1, 'Informatique Fondamentale 5', 5, 'physique'),
(6, 2, 'Informatique TIC 4', 4, 'physique'),
(7, 2, 'Informatique TIC 5', 5, 'physique'),
(8, NULL, 'INFO 4', 4, 'virtuelle'),
(9, NULL, 'INFO 5', 5, 'virtuelle');

-- --------------------------------------------------------

--
-- Structure de la table `composant_classe_virtuelle`
--

CREATE TABLE `composant_classe_virtuelle` (
  `id_classe_virtuelle` int(11) NOT NULL,
  `id_classe_physique` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `composant_classe_virtuelle`
--

INSERT INTO `composant_classe_virtuelle` (`id_classe_virtuelle`, `id_classe_physique`) VALUES
(8, 4),
(8, 6),
(9, 5),
(9, 7);

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

CREATE TABLE `departement` (
  `id_departement` int(11) NOT NULL,
  `nom_departement` varchar(255) DEFAULT NULL,
  `date_creation` date DEFAULT NULL,
  `numero_texte_creation` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id_departement`, `nom_departement`, `date_creation`, `numero_texte_creation`) VALUES
(1, 'Informatique', '2012-04-20', 'AD1/RE/51581/A12');

-- --------------------------------------------------------

--
-- Structure de la table `desinscrire_etudiant_ue`
--

CREATE TABLE `desinscrire_etudiant_ue` (
  `id_desinscription_ue` int(11) NOT NULL,
  `matricule` varchar(10) NOT NULL,
  `code_ue` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `element_constitutif`
--

CREATE TABLE `element_constitutif` (
  `id_element_constitutif` int(11) NOT NULL,
  `code_ec` varchar(15) DEFAULT NULL,
  `nom_ec` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `element_constitutif`
--

INSERT INTO `element_constitutif` (`id_element_constitutif`, `code_ec`, `nom_ec`) VALUES
(1, 'INF411-1', 'Cartographie'),
(2, 'INF421-1', 'Type Abstrait de donnÃ©es'),
(3, 'INF 421-2', 'TP Type Abstrait de donnÃ©es'),
(4, 'INF431-3', 'Architecture et des ordinateurs avancÃ©es'),
(5, 'INF 431-2', 'Programmation Web HTML, CSS, JavaScript'),
(6, 'INF111-1', 'Introduction Ã  l\'algorithmique'),
(7, 'INF111-2', 'Programmation StructurÃ©e en Pascal'),
(8, 'INF121-1', 'Structures de donnÃ©es'),
(9, 'INF121-2', 'Analyse StructurÃ©e'),
(10, 'INF131-1', 'SystÃ¨me d\'information I: MERISE'),
(11, 'INF131-2', 'Bases de donnÃ©es relationelles 1'),
(12, 'PHI141-1', 'MÃ©canique du point'),
(13, 'PHI141-2', 'Introduction Ã  la mÃ©canique du solide'),
(14, 'PHI141-3', 'Electronique GÃ©nÃ©rale'),
(15, 'SED151-1', 'Philosophie Sociologie et histoire de l\'Ã©ducation'),
(16, 'SED151-2', 'Psychologie gÃ©nÃ©rale'),
(17, 'LAN161-1', 'Formation Bilingue'),
(18, 'LAN161-2', 'Techniques d\'analyse des textes en franÃ§ais');

-- --------------------------------------------------------

--
-- Structure de la table `element_grille`
--

CREATE TABLE `element_grille` (
  `id_element_grille` int(11) NOT NULL,
  `id_grille` int(11) DEFAULT NULL,
  `id_element_constitutif` int(11) NOT NULL,
  `code_ue` varchar(10) NOT NULL,
  `id_enseignant` int(11) NOT NULL,
  `pourcentagetp` int(11) DEFAULT NULL,
  `pourcentageprojet` int(11) DEFAULT NULL,
  `pourcentagecc` int(11) DEFAULT NULL,
  `pourcentageexamen` int(11) DEFAULT NULL,
  `credit` int(11) DEFAULT NULL,
  `semestre` int(11) DEFAULT NULL,
  `annee_academique` varchar(10) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `element_grille`
--

INSERT INTO `element_grille` (`id_element_grille`, `id_grille`, `id_element_constitutif`, `code_ue`, `id_enseignant`, `pourcentagetp`, `pourcentageprojet`, `pourcentagecc`, `pourcentageexamen`, `credit`, `semestre`, `annee_academique`, `role`) VALUES
(6, 2, 3, 'INF421', 2, 30, 10, 20, 40, 2, 1, '2018-2019', NULL),
(8, 2, 2, 'INF421', 2, 30, NULL, 20, 50, 3, 2, '2018-2019', NULL),
(9, 4, 1, 'INF411', 2, 20, 10, 20, 50, 2, 1, '2018-2019', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `element_grille_classe`
--

CREATE TABLE `element_grille_classe` (
  `id_element_grille_classe` int(11) NOT NULL,
  `id_element_grille` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `enseignant`
--

CREATE TABLE `enseignant` (
  `id_enseignant` int(11) NOT NULL,
  `nom_prenom` varchar(255) DEFAULT NULL,
  `grade` varchar(15) DEFAULT NULL,
  `numero_cni` varchar(100) DEFAULT NULL,
  `specialite_recherche` text,
  `telephone` varchar(10) DEFAULT NULL,
  `ville_residence` varchar(25) DEFAULT NULL,
  `categorie` varchar(255) DEFAULT NULL,
  `sexe` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `enseignant`
--

INSERT INTO `enseignant` (`id_enseignant`, `nom_prenom`, `grade`, `numero_cni`, `specialite_recherche`, `telephone`, `ville_residence`, `categorie`, `sexe`) VALUES
(1, 'JOHN DOE', 'doctorant', '1738123817', 'Sciences sociales', '637383838', 'Bertoua', 'vacataire', 'M'),
(2, 'Jacika DOE', 'docteur', '1254254545', 'Informatique quantique', '61545152', 'Bertoua', 'permanent', 'F');

-- --------------------------------------------------------

--
-- Structure de la table `enseignant_departement`
--

CREATE TABLE `enseignant_departement` (
  `id_enseignant_departement` int(11) NOT NULL,
  `id_enseignant` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `debut_fonction` date DEFAULT NULL,
  `fin_fonction` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `etudiant`
--

CREATE TABLE `etudiant` (
  `matricule` varchar(10) NOT NULL,
  `nom_prenom` varchar(255) DEFAULT NULL,
  `date_naissance` date DEFAULT NULL,
  `lieu_naissance` varchar(254) DEFAULT NULL,
  `sexe` char(1) DEFAULT NULL,
  `num_cni` varchar(40) DEFAULT NULL,
  `region_origine` varchar(10) DEFAULT NULL,
  `telephone` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone_parent` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudiant`
--

INSERT INTO `etudiant` (`matricule`, `nom_prenom`, `date_naissance`, `lieu_naissance`, `sexe`, `num_cni`, `region_origine`, `telephone`, `email`, `telephone_parent`) VALUES
('17A065EB', 'BOKPILO DOMBOU Valentin', '1993-09-14', 'Ngaoundere', 'M', '157613955', 'Adamaoua', '674006088', 'bokpilodombouvalentin@ensbertoua.cm', '689117510'),
('17A066EB', 'DJANADEK MODO GHISLAINE', '1999-08-03', 'Bertoua', 'F', '161480989', 'Est', '698422996', 'djanadekmodoghislaine@ensbertoua.cm', '691507983'),
('17A067EB', 'ETEME ONANA FRANCIS ROLAND', '1992-10-18', 'Yaounde', 'M', '195148745', 'Est', '651088318', 'etemeonanafrancisroland@ensbertoua.cm', '690714693'),
('17A068EB', 'IBRAHIMA BOBBO', '1991-01-30', 'Ngaoundere', 'M', '114697888', 'Adamaoua', '699449379', 'ibrahimabobbo@ensbertoua.cm', '696892561'),
('17A069EB', 'JIOTSOP  VOUFFO CYRYLLE', '1993-10-20', 'Ndoh Djuttitsa', 'M', '143714298', 'Est', '650304463', 'jiotsopvouffocyrylle@ensbertoua.cm', '698871966'),
('17A070EB', 'KABIYE CHRISTIAN', '1990-01-01', 'Batouri', 'M', '131716893', 'Est', '658797939', 'kabiyechristian@ensbertoua.cm', '672830361'),
('17A071EB', 'MEDOUMBA WOLLA FRANCK', '1989-12-11', 'Yaounde', 'M', '157463176', 'Est', '664041634', 'medoumbawollafranck@ensbertoua.cm', '676963853'),
('17A072EB', 'MEKOMPOMB Yannick', '1994-09-22', 'Atok', 'M', '166784050', 'Est', '659598336', 'mekompombyannick@ensbertoua.cm', '697153419'),
('17A073EB', 'MGBAYAFOU MEFIRE HABIBA', '1994-06-22', 'Yaounde', 'F', '153731196', 'Ouest', '671192233', 'mgbayafoumefirehabiba@ensbertoua.cm', '658280554'),
('17A074EB', 'NOUHOU KALI', '1993-06-15', 'Lagdo', 'M', '125842152', 'Extreme-No', '671715704', 'nouhoukali@ensbertoua.cm', '658252634'),
('17A179EB', 'DAOUDOU GABRIEL', '1998-03-28', 'Garoua', 'M', '134467566', 'Est', '680893363', 'daoudougabriel@ensbertoua.cm', '660071257'),
('17A180EB', 'EKANI ATIMILE ANDRE CEDRIC', '1996-06-01', 'Mindourou', 'M', '156301581', 'Est', '676351022', 'ekaniatimileandrecedric@ensbertoua.cm', '658267965'),
('17A181EB', 'HAMIDOU IBRAHIM', '1995-10-21', 'Mandjou-Bertoua', 'M', '193080366', 'Est', '699513333', 'hamidouibrahim@ensbertoua.cm', '692974458'),
('17A182EB', 'HOUSSEINI MOHAMADOU', '1998-01-01', 'Malarba 2', 'M', '191513391', 'Nord', '673656357', 'housseinimohamadou@ensbertoua.cm', '663686416'),
('17A183EB', 'MONY MONEYANG JADE NICOLE', '1999-02-28', 'Yaounde', 'F', '138195075', 'Sud', '659897233', 'monymoneyangjadenicole@ensbertoua.cm', '698230465'),
('17B062EB', 'ABSATOU', '1990-07-15', 'Kentzou', 'F', '128332509', 'Est', '685409383', 'absatou@ensbertoua.cm', '697476030'),
('17B063EB', 'LOYAKBA YANENBO PELEG', '1995-03-12', 'Garoua', 'M', '155727283', 'Nord', '673708852', 'loyakbayanenbopeleg@ensbertoua.cm', '660024005'),
('17B064EB', 'MENKANDE EDOUARD PAULIN', '1986-01-29', 'Doume', 'M', '138725159', 'Est', '650350967', 'menkandeedouardpaulin@ensbertoua.cm', '687310248'),
('17B065EB', 'MOHAMADOU', '1990-02-22', 'Ngaoundere', 'M', '128296945', 'Adamaoua', '652858194', 'mohamadou@ensbertoua.cm', '658317926'),
('17B066EB', 'SATOUDOU NGONO NADEGE', '1990-03-24', 'Yaounde', 'F', '151934442', 'Centre', '669408988', 'satoudoungononadege@ensbertoua.cm', '699614146'),
('17B067EB', 'ASSOAH FIDELE', '1986-12-14', 'Sibita-Doume', 'M', '139901609', 'Est', '658610610', 'assoahfidele@ensbertoua.cm', '663407050'),
('17B068EB', 'BILONO BALOULO HERMANN PATRICK', '1992-09-02', 'Mopouo', 'M', '189665492', 'Est', '673447046', 'bilonobaloulohermannpatrick@ensbertoua.cm', '690320325'),
('17B069EB', 'EWAH SAMSOM', '1986-06-16', 'Ndokou', 'M', '149875812', 'Littoral', '661084889', 'ewahsamsom@ensbertoua.cm', '681875389'),
('17B070EB', 'HAYANA BOUKAR', '1995-02-08', 'Maroua', 'F', '188589059', 'Extreme-No', '651655199', 'hayanaboukar@ensbertoua.cm', '672007888'),
('17B071EB', 'KEN SOLANGE NEH', '1986-06-14', 'Nkambe', 'F', '179761432', 'Nord-Ouest', '667914846', 'kensolangeneh@ensbertoua.cm', '675226404'),
('17B072EB', 'MBA AVEBE JEAN-PIERRE', '1995-01-02', 'Yaounde', 'M', '186288715', 'Centre', '673158790', 'mbaavebejean-pierre@ensbertoua.cm', '685872298'),
('17B073EB', 'MBA\'A PATIENCE', '1986-10-07', 'Metet', 'F', '152931898', 'Centre', '697279649', 'mba\'apatience@ensbertoua.cm', '655869365'),
('17B074EB', 'SABELA BOGAM AUXENCE', '1992-03-24', 'Mambaya', 'M', '188732553', 'Est', '664739891', 'sabelabogamauxence@ensbertoua.cm', '694758807'),
('17B075EB', 'SONINGA ACHILLE ROSNY', '1992-11-23', 'Dimako', 'M', '152054771', 'Est', '660150075', 'soningaachillerosny@ensbertoua.cm', '668302114'),
('17B091EB', 'ADJI ALI BOUBAKARI ', '1996-11-14', 'Mora', 'M', '149218570', NULL, '651747759', 'adjialiboubakari@ensbertoua.cm', '696561725'),
('17B092EB', 'DAMA JEAN', '1993-08-19', 'Gambour', 'M', '196070125', NULL, '690151928', 'damajean@ensbertoua.cm', '674301694'),
('17B093EB', 'HAMAN ABA ANDRE YANNICK', '1995-09-23', 'Bertoua', 'M', '153823016', NULL, '668420627', 'hamanabaandreyannick@ensbertoua.cm', '672003630'),
('17B094EB', 'KOUER JOEL PLACIDE', '1988-06-11', 'Ngovayang', 'M', '128646622', NULL, '669708897', 'kouerjoelplacide@ensbertoua.cm', '653873513'),
('17B095EB', 'MOHAMADOU HAFIZ', '1996-04-08', 'Ngaoundere', 'M', '144221604', NULL, '659220979', 'mohamadouhafiz@ensbertoua.cm', '682531512'),
('17B097EB', 'NGONO BERTRAND RODRIGUE', '1987-08-18', 'Ebang Minala', 'M', '113350093', NULL, '655028283', 'ngonobertrandrodrigue@ensbertoua.cm', '696600854'),
('17B098EB', 'ELLA MINSONGO JEAN RENE', '1987-12-25', 'Sangmelima', 'M', '161244576', 'Sud', '662418007', 'ellaminsongojeanrene@ensbertoua.cm', '693488987'),
('17B137EB', 'AGNANGMA HUGUES ALEXIS', '1987-06-01', 'Yaounde', 'M', '112841884', 'Centre', '657193440', 'agnangmahuguesalexis@ensbertoua.cm', '680434821'),
('17B138EB', 'AZEBAZE MARTIN CLAUDEL', '1986-02-24', 'Mbouda', 'M', '114059292', 'Ouest', '652638845', 'azebazemartinclaudel@ensbertoua.cm', '656325086'),
('17B139EB', 'BANA MBANA EMENE PATRICK JOEL', '1985-04-11', 'Bertoua', 'M', '145196978', 'Centre', '650615957', 'banambanaemenepatrickjoel@ensbertoua.cm', '690553760'),
('17B140EB', 'MENDOUGA MEZANG CHRISTIAN KEVIN', '1992-05-02', 'Yaounde', 'M', '121443847', 'Est', '669084403', 'mendougamezangchristiankevin@ensbertoua.cm', '650514161'),
('17B141EB', 'NTOKANDA PETELE PHILIPPE', '1986-04-09', 'Yokadouma', 'M', '122192681', 'Est', '688642661', 'ntokandapetelephilippe@ensbertoua.cm', '682363753'),
('17B142EB', 'NGNIADO KALEGAM Emmanuel', '1989-04-01', 'Yaounde', 'M', '182078852', 'Ouest', '660845694', 'ngniadokalegamemmanuel@ensbertoua.cm', '657647003'),
('17B143EB', 'NKAPIENG CHRISTINE PULCHERIE', '1988-02-19', 'Yaounde', 'F', '147477492', 'Ouest', '664227729', 'nkapiengchristinepulcherie@ensbertoua.cm', '697624944'),
('17B144EB', 'TSOUNG KOMBEUL NADINE JOELLE', '1987-08-15', 'Douala', 'F', '122525731', 'Est', '692900752', 'tsoungkombeulnadinejoelle@ensbertoua.cm', '684686389'),
('17B149EB', 'BASSIROU MANA', '1995-11-12', 'Pina', 'M', '190830222', '', '657555237', 'bassiroumana@ensbertoua.cm', '657957762'),
('17B150EB', 'EPANE NZIE NORBERT', '1995-06-15', 'Yaounde', 'M', '187995654', NULL, '687139726', 'epanenzienorbert@ensbertoua.cm', '651223753'),
('17B151EB', 'OUMAROU SAIDOU', '1996-12-10', 'Bogo', 'M', '124450329', NULL, '691421092', 'oumarousaidou@ensbertoua.cm', '676882499'),
('18A094EB', 'DJEUKA TSIGUIA ARSENE', '1997-06-20', 'Dschang', 'M', '192195420', 'OUEST', '690861259', 'djeukatsiguiaarsene@ensbertoua.cm', '650246745'),
('18A095EB', 'ESSABIALIAM KAMBONG BRICE LANDRY', '2001-03-18', 'Bertoua', 'M', '168043306', 'Est', '663189158', 'essabialiamkambongbricelandry@ensbertoua.cm', '678357703'),
('18A096EB', 'FOKOU KANA LAURES', '2000-06-08', 'Ngaoundere', 'M', '165640074', 'Ouest', '662179478', 'fokoukanalaures@ensbertoua.cm', '681521106'),
('18A097EB', 'KENFACK SORGOUM VICTOR JOEL', '1993-01-18', 'Bafou', 'M', '179735487', 'Est', '666515577', 'kenfacksorgoumvictorjoel@ensbertoua.cm', '673092141'),
('18A098EB', 'KONGNI KAMTA LARISSA LOUISIANE', '1992-05-14', 'Douala', 'F', '117339186', 'Ouest', '671682026', 'kongnikamtalarissalouisiane@ensbertoua.cm', '664113509'),
('18A099EB', 'KOUAM KAMWA CHATEL STEPHANE', '1992-02-19', 'Baham', 'M', '123870832', 'Ouest', '695317772', 'kouamkamwach?telst?phane@ensbertoua.cm', '697667165'),
('18A100EB', 'KOUSSO MEKONTSOU BORIS', '1996-03-18', 'Mbouda', 'M', '146320825', 'Est', '655831017', 'koussomekontsouboris@ensbertoua.cm', '680797528'),
('18A101EB', 'MBE CORNEILLE MURIAN', '1991-08-13', 'Kalate', 'M', '126438330', 'Sud', '686430877', 'mbecorneillemurian@ensbertoua.cm', '655213413'),
('18A102EB', 'MBOCK NGUEND II MOISE WEGENER', '1995-06-23', 'Eseka', 'M', '196703842', 'Centre', '692142470', 'mbocknguendiimoisewegener@ensbertoua.cm', '690849305'),
('18A103EB', 'MENDONGO ROBERT MAXIME', '1995-02-15', 'Batouri', 'M', '195297880', 'Est', '679011120', 'mendongorobertmaxime@ensbertoua.cm', '693423165'),
('18A104EB', 'NGANSOP MBAKOP ARNOLD', '1995-02-06', 'Bafoussam', 'M', '111127208', 'Est', '688251350', 'ngansopmbakoparnold@ensbertoua.cm', '666737822'),
('18A105EB', 'NGO NLEND CHRISTELLE LARISSA', '1993-11-04', 'Mfou', 'F', '149387344', 'Littoral', '688875598', 'ngonlendchristellelarissa@ensbertoua.cm', '651848936'),
('18A106EB', 'NGOMO DONY ARSENE', '1997-01-11', 'Abong-Mbang', 'M', '193334484', 'Est', '670955644', 'ngomodonyarsene@ensbertoua.cm', '670798311'),
('18A107EB', 'NOUHOU ALIOUM', '1997-04-12', 'Maroua', 'M', '140124982', 'Extreme-No', '683048522', 'nouhoualioum@ensbertoua.cm', '677153339'),
('18A108EB', 'TICHE JESSY CARELLE', '1990-04-07', 'Batie', 'F', '174652895', 'Ouest', '655125118', 'tichejessycarelle@ensbertoua.cm', '651380654'),
('18A255EB', 'DAWAÏ BOUBA', '1990-08-07', 'Bouba Djara Guider', 'M', '136105131', 'Nord', '698261630', 'dawabouba@ensbertoua.cm', '650509198'),
('18A256EB', 'ABOUBAKAR ABDOU SALAM', '1998-07-12', 'Danfili', 'M', '149253098', 'Adamaoua', '651726296', 'aboubakarabdousalam@ensbertoua.cm', '691304276'),
('18B100EB', 'ATANA DYLANE', '1998-12-24', 'Nguelemendouka', 'M', '150375470', 'Est', '679061181', 'atanadylane@ensbertoua.cm', '651855656'),
('18B101EB', 'BENGA ALICE LETITIA', '1998-12-26', 'Sangmelima', 'F', '176082287', 'Sud', '672539981', 'bengaaliceletitia@ensbertoua.cm', '684526763'),
('18B102EB', 'ITOLI OUNEMBOUNI ALEX', '1993-07-07', 'Ebolowa', 'M', '176253982', 'Centre', '690498178', 'itoliounembounialex@ensbertoua.cm', '653096054'),
('18B103EB', 'KENGNE MBA DIANE LAURIANE', '1998-07-02', 'Penka-Michel', 'F', '184331117', 'Ouest', '677371651', 'kengnembadianelauriane@ensbertoua.cm', '665631170'),
('18B104EB', 'NITCHEU TCHUISSI JOSEPH PARFAIT', '1996-04-10', 'Loum', 'M', '185883931', 'Sud-Ouest', '684608015', 'nitcheutchuissijosephparfait@ensbertoua.cm', '691998563'),
('18B105EB', 'TALATALA GRAINGE RODRIGUE', '1990-07-22', 'Ebolowa', 'M', '159319250', 'Est', '680944269', 'talatalagraingerodrigue@ensbertoua.cm', '689190023'),
('18B106EB', 'TONYE NSEGBE MARC BLAISE', '1996-05-13', 'Yaounde', 'M', '167878274', 'Centre', '695370715', 'tonyensegbemarcblaise@ensbertoua.cm', '678877770'),
('18B107EB', 'WAI WANTOUNA NDANGA BIENVENUE', '1996-07-29', 'Bertoua', 'M', '171497849', 'Est', '676272498', 'waiwantounandangabienvenue@ensbertoua.cm', '675014042'),
('18B108EB', 'AKAMBA NICOLE VANESSA FERNANDE', '1991-06-22', 'Zoetele', 'F', '161455887', 'Sud', '694579884', 'akambanicolevanessafernande@ensbertoua.cm', '672766702'),
('18B109EB', 'AYEMAFO NWATI GERVAISE', '1988-06-02', 'Bangang', 'F', '130770257', 'Ouest', '688041297', 'ayemafonwatigervaise@ensbertoua.cm', '650182082'),
('18B110EB', 'DJOUMESSI GOUFACK SYLVIANE', '1987-01-22', 'Dschang', 'F', '172860617', 'Ouest', '688364392', 'djoumessigoufacksylviane@ensbertoua.cm', '656545975'),
('18B111EB', 'DOUNTIO DJASSI MURIELLE', '1995-07-23', 'Bandjoun', 'F', '161056936', 'Ouest', '683406196', 'dountiodjassimurielle@ensbertoua.cm', '659029180'),
('18B112EB', 'FADIMATOU', '1987-08-22', 'Touboro', 'F', '184610098', 'Extreme-No', '673616046', 'fadimatou@ensbertoua.cm', '658197777'),
('18B113EB', 'MBO NADINE WILLY', '1986-08-15', 'Douala', 'F', '157778112', 'Sud', '669430922', 'mbonadinewilly@ensbertoua.cm', '697205624'),
('18B114EB', 'MBOUGANG PAUL FELIX', '1989-11-16', 'Yaounde', 'M', '127685564', 'Ouest', '668058197', 'mbougangpaulfelix@ensbertoua.cm', '675872074'),
('18B115EB', 'NGOUCHEKEN ADJARA', '1986-06-25', 'Foumbot', 'F', '143855623', 'Ouest', '676815063', 'ngouchekenadjara@ensbertoua.cm', '696243974'),
('18B116EB', 'SANDJONG FOMENI JOYANIE MONELLE', '1993-03-08', 'Loum', 'F', '134715619', 'Ouest', '695857033', 'sandjongfomenijoyaniemonelle@ensbertoua.cm', '689434588'),
('18B117EB', 'TEMGOUA ZEFFACK CHRISTIAN LAUREN', '1990-02-15', 'Dschang', 'M', '149466547', 'Ouest', '658703996', 'temgouazeffackchristianlauren@ensbertoua.cm', '675899029'),
('18B118EB', 'FOKOU WOJOUOMBO DIDIER', '1987-03-30', 'Foumbot', 'M', '117541199', 'Ouest', '691366531', 'fokouwojouombodidier@ensbertoua.cm', '666604583'),
('18B119EB', 'KENGUE NGOUENE JOCELYN DIRANE', '1995-08-12', 'Yaounde', 'M', '123409620', 'Ouest', '674742568', 'kenguengouenejocelyndirane@ensbertoua.cm', '685339060'),
('18B120EB', 'NKONGA LILIANE', '1987-04-14', 'Tonga', 'F', '160138029', 'Centre', '659584579', 'nkongaliliane@ensbertoua.cm', '669810270'),
('18B121EB', 'NOAH ABINI DEMAISON', '1989-02-25', 'Akono', 'M', '137492941', 'Centre', '650973308', 'noahabinidemaison@ensbertoua.cm', '652292271'),
('18B233EB', 'OBAMA OTTOU JOSEPH OLIVIER THIER', '1986-09-19', 'Yaounde', 'M', '116351986', 'Centre', '692881302', 'obamaottoujosepholivierthier@ensbertoua.cm', '673220436'),
('18B234EB', 'MPOE DJODJOL LILIANE LAURE', '1991-02-09', 'Yaounde', 'F', '194772460', 'Est', '656879665', 'mpoedjodjollilianelaure@ensbertoua.cm', '692046487');

-- --------------------------------------------------------

--
-- Structure de la table `filiere`
--

CREATE TABLE `filiere` (
  `id_filiere` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `code_filliere` varchar(15) DEFAULT NULL,
  `nom_filiere` varchar(250) DEFAULT NULL,
  `cycle` varchar(15) DEFAULT NULL,
  `date_creation` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `filiere`
--

INSERT INTO `filiere` (`id_filiere`, `id_departement`, `code_filliere`, `nom_filiere`, `cycle`, `date_creation`) VALUES
(1, 1, 'FON', 'Informatique Fondamentale', 'second', '2012-04-20'),
(2, 1, 'TIC', 'Informatique TIC', 'premier', '2012-04-20'),
(3, 1, 'INF', 'Informatique', 'premier', '2012-04-20');

-- --------------------------------------------------------

--
-- Structure de la table `grille`
--

CREATE TABLE `grille` (
  `id_grille` int(11) NOT NULL,
  `id_classe` int(11) NOT NULL,
  `annee_academique` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `grille`
--

INSERT INTO `grille` (`id_grille`, `id_classe`, `annee_academique`) VALUES
(2, 4, '2018-2019'),
(3, 1, '2018-2019'),
(4, 8, '2018-2019');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

CREATE TABLE `inscription` (
  `id_inscription` int(11) NOT NULL,
  `matricule` varchar(10) NOT NULL,
  `id_classe` int(11) NOT NULL,
  `annee_academique` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`id_inscription`, `matricule`, `id_classe`, `annee_academique`) VALUES
(1, '18B106EB', 4, '2018-2019'),
(2, '17A065EB', 2, '2018-2019'),
(3, '17A066EB', 2, '2018-2019'),
(4, '17A067EB', 2, '2018-2019'),
(5, '17A068EB', 2, '2018-2019'),
(6, '17A069EB', 2, '2018-2019'),
(7, '17A070EB', 2, '2018-2019'),
(8, '17A071EB', 2, '2018-2019'),
(9, '17A072EB', 2, '2018-2019'),
(10, '17A073EB', 2, '2018-2019'),
(11, '17A074EB', 2, '2018-2019'),
(12, '17A179EB', 2, '2018-2019'),
(13, '17A180EB', 2, '2018-2019'),
(14, '17A181EB', 2, '2018-2019'),
(15, '18B104EB', 4, '2018-2019'),
(16, '18B103EB', 4, '2018-2019'),
(17, '18B119EB', 6, '2018-2019'),
(18, '18B116EB', 6, '2018-2019'),
(19, '18B110EB', 6, '2018-2019'),
(20, '18A096EB', 1, '2018-2019'),
(21, '18A099EB', 1, '2018-2019'),
(22, '18A102EB', 1, '2018-2019'),
(23, '17A183EB', 2, '2018-2019'),
(24, '17A182EB', 5, '2018-2019'),
(27, '17B062EB', 4, '2018-2019'),
(28, '18B100EB', 4, '2018-2019'),
(29, '18B105EB', 4, '2018-2019'),
(30, '17B063EB', 5, '2018-2019'),
(31, '17B064EB', 5, '2018-2019'),
(32, '17B065EB', 5, '2018-2019'),
(33, '17B067EB', 7, '2018-2019');

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `date_heure` datetime DEFAULT NULL,
  `action` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `note_composition`
--

CREATE TABLE `note_composition` (
  `id_note_composition` int(11) NOT NULL,
  `matricule` varchar(10) NOT NULL,
  `id_element_grille` int(11) NOT NULL,
  `note` float DEFAULT NULL,
  `type_composition` char(255) DEFAULT NULL,
  `date_composition` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `note_composition`
--

INSERT INTO `note_composition` (`id_note_composition`, `matricule`, `id_element_grille`, `note`, `type_composition`, `date_composition`) VALUES
(1, '18B103EB', 8, 1, 'SN', '2012-12-14'),
(2, '18B104EB', 8, 1, 'SN', '2012-12-14'),
(3, '18B106EB', 8, 1, 'SN', '2012-12-14'),
(4, '18B103EB', 6, 1, 'TP', '2020-03-16'),
(5, '18B104EB', 6, 1, 'TP', '2020-03-16'),
(6, '18B106EB', 6, 1, 'TP', '2020-03-16'),
(7, '17B062EB', 6, 1, 'SN', '2012-12-14'),
(8, '18B100EB', 6, 1, 'SN', '2012-12-14'),
(9, '18B103EB', 6, 1, 'SN', '2012-12-14'),
(10, '18B104EB', 6, 1, 'SN', '2012-12-14'),
(11, '18B105EB', 6, 1, 'SN', '2012-12-14'),
(12, '18B106EB', 6, 1, 'SN', '2012-12-14'),
(13, '17B062EB', 8, 1, 'SN', '2012-12-14'),
(71, '18B100EB', 8, 1, 'TP', '1996-05-14'),
(72, '18B100EB', 8, 1, 'TP', '1996-05-14'),
(73, '18B104EB', 8, 1, 'TP', '1996-05-14'),
(74, '18B105EB', 8, 1, 'TP', '1996-02-12'),
(75, '18B105EB', 8, 1, 'CC', '1996-02-12'),
(76, '18B105EB', 8, 1, 'SN', '1996-02-12'),
(77, '18B105EB', 9, 14.8, 'TPE', '2020-03-25'),
(78, '18B105EB', 9, 12, 'SN', '2020-03-25'),
(79, '18B106EB', 9, 15, 'TPE', '2020-03-25'),
(80, '18B110EB', 9, 12, 'TPE', '2020-03-25');

-- --------------------------------------------------------

--
-- Structure de la table `privilege`
--

CREATE TABLE `privilege` (
  `id_privilege` int(11) NOT NULL,
  `nom_privilege` varchar(50) DEFAULT NULL,
  `type_privilege` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `privilege_profil`
--

CREATE TABLE `privilege_profil` (
  `id_profil` int(11) NOT NULL,
  `id_privilege` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `profil`
--

CREATE TABLE `profil` (
  `id_profil` int(11) NOT NULL,
  `nom_profil` varchar(25) DEFAULT NULL,
  `type_profil` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `profil_utilisateur`
--

CREATE TABLE `profil_utilisateur` (
  `id_profil_utilisateur` int(11) NOT NULL,
  `id_profil` int(11) NOT NULL,
  `id_utilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `reinscrire_etudiant_ue`
--

CREATE TABLE `reinscrire_etudiant_ue` (
  `id_reinscription_ue` int(11) NOT NULL,
  `matricule` varchar(10) NOT NULL,
  `code_ue` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `unite_enseignement`
--

CREATE TABLE `unite_enseignement` (
  `code_ue` varchar(10) NOT NULL,
  `id_enseignant` int(11) NOT NULL,
  `nom_ue` varchar(255) DEFAULT NULL,
  `type` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `unite_enseignement`
--

INSERT INTO `unite_enseignement` (`code_ue`, `id_enseignant`, `nom_ue`, `type`) VALUES
('FON441', 1, 'ALGORITHMIQUE AVANCEE ET INTELLIGENCE ARTIFICIELLE', 'fondamentale'),
('FON461', 1, 'CALCUL SCIENTIFIQUE ET THEORIE DES CATEGORIES', 'fondamentale'),
('INF111', 2, 'ALGORITHMIQUE ET PROGRAMMATION', 'fondamentale'),
('INF121', 2, 'STRUCTURES DE DONNEES ET ANALYSE STRUCTUREES', 'fondamentale'),
('INF131', 2, 'SYSTEME D\'INFORMATION ET BASE DE DONNEES 1', 'fondamentale'),
('INF411', 1, 'SYSTEME D\'INFORMATION SIG ET CARTOGRAPHIE', 'fondamentale'),
('INF421', 1, 'TYPE ABSTRAIT DE DONNEE', 'fondamentale'),
('INF431', 1, 'ENVIRONNEMENT INFORMATIQUE ET PROGRAMMATION WEB', 'fondamentale'),
('LAN161', 2, 'LANGUES', 'complementaire'),
('PHI141', 2, 'MECANIQUE ET ELECTRONIQUE', 'complementaire'),
('SED 452', 1, 'Enseignement generalisÃ©', 'complementaire'),
('SED151', 2, 'PHILOSOPHIE, SOCIOLOGIE, HISTOIRE DE L\'EDUCATION ET PSYCHOLOGIE GENERALE', 'professionelle'),
('SED451', 1, 'PSYCHOLOGIE PHILOSOPHIE, SOCIOLOGIE ET HISTOIRE DE L\'EDUCATION', 'professionelle');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id_utilisateur` int(11) NOT NULL,
  `id_enseignant` int(11) DEFAULT NULL,
  `special_id` int(11) DEFAULT NULL,
  `matricule` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` varchar(10) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `etat` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `id_enseignant`, `special_id`, `matricule`, `email`, `username`, `password`, `telephone`, `role`, `etat`) VALUES
(1, NULL, NULL, NULL, 'admin@sagge-notes.cm', 'admin', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '673801800', 'admin', 'active'),
(2, NULL, NULL, '18B106EB', NULL, '18B106EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', NULL, 'etudiant', 'active'),
(3, 1, 1, NULL, 'john@doe.com', 'john@doe.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '637383838', 'enseignant', 'active'),
(4, 2, 2, NULL, 'doe@jacika.com', 'doe@jacika.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '61545152', 'enseignant', 'active'),
(5, 0, 0, '17A065EB', 'bokpilodombouvalentin@ensbertoua.cm', '17A065EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', '674006088', 'etudiant', 'active'),
(6, 0, 0, '17A066EB', 'djanadekmodoghislaine@ensbertoua.cm', '17A066EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd9', '698422996', 'etudiant', 'active'),
(7, 0, 0, '17A067EB', 'etemeonanafrancisroland@ensbertoua.cm', '17A067EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd10', '651088318', 'etudiant', 'active'),
(8, 0, 0, '17A068EB', 'ibrahimabobbo@ensbertoua.cm', '17A068EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd11', '699449379', 'etudiant', 'active'),
(9, 0, 0, '17A069EB', 'jiotsopvouffocyrylle@ensbertoua.cm', '17A069EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd12', '650304463', 'etudiant', 'active'),
(10, 0, 0, '17A070EB', 'kabiyechristian@ensbertoua.cm', '17A070EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd13', '658797939', 'etudiant', 'active'),
(11, 0, 0, '17A071EB', 'medoumbawollafranck@ensbertoua.cm', '17A071EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd14', '664041634', 'etudiant', 'active'),
(12, 0, 0, '17A072EB', 'mekompombyannick@ensbertoua.cm', '17A072EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd15', '659598336', 'etudiant', 'active'),
(13, 0, 0, '17A073EB', 'mgbayafoumefirehabiba@ensbertoua.cm', '17A073EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd16', '671192233', 'etudiant', 'active'),
(14, 0, 0, '17A074EB', 'nouhoukali@ensbertoua.cm', '17A074EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd17', '671715704', 'etudiant', 'active'),
(15, 0, 0, '17A179EB', 'daoudougabriel@ensbertoua.cm', '17A179EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd18', '680893363', 'etudiant', 'active'),
(16, 0, 0, '17A180EB', 'ekaniatimileandrecedric@ensbertoua.cm', '17A180EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd19', '676351022', 'etudiant', 'active'),
(17, 0, 0, '17A181EB', 'hamidouibrahim@ensbertoua.cm', '17A181EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd20', '699513333', 'etudiant', 'active'),
(18, 0, 0, '17A182EB', 'housseinimohamadou@ensbertoua.cm', '17A182EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd21', '673656357', 'etudiant', 'active'),
(19, 0, 0, '17A183EB', 'monymoneyangjadenicole@ensbertoua.cm', '17A183EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd22', '659897233', 'etudiant', 'active'),
(20, 0, 0, '17B062EB', 'absatou@ensbertoua.cm', '17B062EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd23', '685409383', 'etudiant', 'active'),
(21, 0, 0, '17B063EB', 'loyakbayanenbopeleg@ensbertoua.cm', '17B063EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd24', '673708852', 'etudiant', 'active'),
(22, 0, 0, '17B064EB', 'menkandeedouardpaulin@ensbertoua.cm', '17B064EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd25', '650350967', 'etudiant', 'active'),
(23, 0, 0, '17B065EB', 'mohamadou@ensbertoua.cm', '17B065EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd26', '652858194', 'etudiant', 'active'),
(24, 0, 0, '17B066EB', 'satoudoungononadege@ensbertoua.cm', '17B066EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd27', '669408988', 'etudiant', 'active'),
(25, 0, 0, '17B067EB', 'assoahfidele@ensbertoua.cm', '17B067EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd28', '658610610', 'etudiant', 'active'),
(26, 0, 0, '17B068EB', 'bilonobaloulohermannpatrick@ensbertoua.cm', '17B068EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd29', '673447046', 'etudiant', 'active'),
(27, 0, 0, '17B069EB', 'ewahsamsom@ensbertoua.cm', '17B069EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd30', '661084889', 'etudiant', 'active'),
(28, 0, 0, '17B070EB', 'hayanaboukar@ensbertoua.cm', '17B070EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd31', '651655199', 'etudiant', 'active'),
(29, 0, 0, '17B071EB', 'kensolangeneh@ensbertoua.cm', '17B071EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd32', '667914846', 'etudiant', 'active'),
(30, 0, 0, '17B072EB', 'mbaavebejean-pierre@ensbertoua.cm', '17B072EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd33', '673158790', 'etudiant', 'active'),
(31, 0, 0, '17B073EB', 'mbaapatience@ensbertoua.cm', '17B073EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd34', '697279649', 'etudiant', 'active'),
(32, 0, 0, '17B074EB', 'sabelabogamauxence@ensbertoua.cm', '17B074EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd35', '664739891', 'etudiant', 'active'),
(33, 0, 0, '17B075EB', 'soningaachillerosny@ensbertoua.cm', '17B075EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd36', '660150075', 'etudiant', 'active'),
(34, 0, 0, '17B091EB', 'adjialiboubakari@ensbertoua.cm', '17B091EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd37', '651747759', 'etudiant', 'active'),
(35, 0, 0, '17B092EB', 'damajean@ensbertoua.cm', '17B092EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd38', '690151928', 'etudiant', 'active'),
(36, 0, 0, '17B093EB', 'hamanabaandreyannick@ensbertoua.cm', '17B093EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd39', '668420627', 'etudiant', 'active'),
(37, 0, 0, '17B094EB', 'kouerjoelplacide@ensbertoua.cm', '17B094EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd40', '669708897', 'etudiant', 'active'),
(38, 0, 0, '17B095EB', 'mohamadouhafiz@ensbertoua.cm', '17B095EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd41', '659220979', 'etudiant', 'active'),
(39, 0, 0, '17B097EB', 'ngonobertrandrodrigue@ensbertoua.cm', '17B097EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd42', '655028283', 'etudiant', 'active'),
(40, 0, 0, '17B098EB', 'ellaminsongojeanrene@ensbertoua.cm', '17B098EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd43', '662418007', 'etudiant', 'active'),
(41, 0, 0, '17B137EB', 'agnangmahuguesalexis@ensbertoua.cm', '17B137EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd44', '657193440', 'etudiant', 'active'),
(42, 0, 0, '17B138EB', 'azebazemartinclaudel@ensbertoua.cm', '17B138EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd45', '652638845', 'etudiant', 'active'),
(43, 0, 0, '17B139EB', 'banambanaemenepatrickjoel@ensbertoua.cm', '17B139EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd46', '650615957', 'etudiant', 'active'),
(44, 0, 0, '17B140EB', 'mendougamezangchristiankevin@ensbertoua.cm', '17B140EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd47', '669084403', 'etudiant', 'active'),
(45, 0, 0, '17B141EB', 'ntokandapetelephilippe@ensbertoua.cm', '17B141EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd48', '688642661', 'etudiant', 'active'),
(46, 0, 0, '17B142EB', 'ngniadokalegamemmanuel@ensbertoua.cm', '17B142EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd49', '660845694', 'etudiant', 'active'),
(47, 0, 0, '17B143EB', 'nkapiengchristinepulcherie@ensbertoua.cm', '17B143EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd50', '664227729', 'etudiant', 'active'),
(48, 0, 0, '17B144EB', 'tsoungkombeulnadinejoelle@ensbertoua.cm', '17B144EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd51', '692900752', 'etudiant', 'active'),
(49, 0, 0, '17B149EB', 'bassiroumana@ensbertoua.cm', '17B149EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd52', '657555237', 'etudiant', 'active'),
(50, 0, 0, '17B150EB', 'epanenzienorbert@ensbertoua.cm', '17B150EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd53', '687139726', 'etudiant', 'active'),
(51, 0, 0, '17B151EB', 'oumarousaidou@ensbertoua.cm', '17B151EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd54', '691421092', 'etudiant', 'active'),
(52, 0, 0, '18A094EB', 'djeukatsiguiaarsene@ensbertoua.cm', '18A094EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd55', '690861259', 'etudiant', 'active'),
(53, 0, 0, '18A095EB', 'essabialiamkambongbricelandry@ensbertoua.cm', '18A095EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd56', '663189158', 'etudiant', 'active'),
(54, 0, 0, '18A096EB', 'fokoukanalaures@ensbertoua.cm', '18A096EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd57', '662179478', 'etudiant', 'active'),
(55, 0, 0, '18A097EB', 'kenfacksorgoumvictorjoel@ensbertoua.cm', '18A097EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd58', '666515577', 'etudiant', 'active'),
(56, 0, 0, '18A098EB', 'kongnikamtalarissalouisiane@ensbertoua.cm', '18A098EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd59', '671682026', 'etudiant', 'active'),
(57, 0, 0, '18A099EB', 'kouamkamwach?telst?phane@ensbertoua.cm', '18A099EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd60', '695317772', 'etudiant', 'active'),
(58, 0, 0, '18A100EB', 'koussomekontsouboris@ensbertoua.cm', '18A100EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd61', '655831017', 'etudiant', 'active'),
(59, 0, 0, '18A101EB', 'mbecorneillemurian@ensbertoua.cm', '18A101EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd62', '686430877', 'etudiant', 'active'),
(60, 0, 0, '18A102EB', 'mbocknguendiimoisewegener@ensbertoua.cm', '18A102EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd63', '692142470', 'etudiant', 'active'),
(61, 0, 0, '18A103EB', 'mendongorobertmaxime@ensbertoua.cm', '18A103EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd64', '679011120', 'etudiant', 'active'),
(62, 0, 0, '18A104EB', 'ngansopmbakoparnold@ensbertoua.cm', '18A104EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd65', '688251350', 'etudiant', 'active'),
(63, 0, 0, '18A105EB', 'ngonlendchristellelarissa@ensbertoua.cm', '18A105EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd66', '688875598', 'etudiant', 'active'),
(64, 0, 0, '18A106EB', 'ngomodonyarsene@ensbertoua.cm', '18A106EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd67', '670955644', 'etudiant', 'active'),
(65, 0, 0, '18A107EB', 'nouhoualioum@ensbertoua.cm', '18A107EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd68', '683048522', 'etudiant', 'active'),
(66, 0, 0, '18A108EB', 'tichejessycarelle@ensbertoua.cm', '18A108EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd69', '655125118', 'etudiant', 'active'),
(67, 0, 0, '18A255EB', 'dawa?bouba@ensbertoua.cm', '18A255EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd70', '698261630', 'etudiant', 'active'),
(68, 0, 0, '18A256EB', 'aboubakarabdousalam@ensbertoua.cm', '18A256EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd71', '651726296', 'etudiant', 'active'),
(69, 0, 0, '18B100EB', 'atanadylane@ensbertoua.cm', '18B100EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd72', '679061181', 'etudiant', 'active'),
(70, 0, 0, '18B101EB', 'bengaaliceletitia@ensbertoua.cm', '18B101EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd73', '672539981', 'etudiant', 'active'),
(71, 0, 0, '18B102EB', 'itoliounembounialex@ensbertoua.cm', '18B102EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd74', '690498178', 'etudiant', 'active'),
(72, 0, 0, '18B103EB', 'kengnembadianelauriane@ensbertoua.cm', '18B103EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd75', '677371651', 'etudiant', 'active'),
(73, 0, 0, '18B104EB', 'nitcheutchuissijosephparfait@ensbertoua.cm', '18B104EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd76', '684608015', 'etudiant', 'active'),
(74, 0, 0, '18B105EB', 'talatalagraingerodrigue@ensbertoua.cm', '18B105EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd77', '680944269', 'etudiant', 'active'),
(75, 0, 0, '18B106EB', 'tonyensegbemarcblaise@ensbertoua.cm', '18B106EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd78', '695370715', 'etudiant', 'active'),
(76, 0, 0, '18B107EB', 'wai?wantounandangabienvenue@ensbertoua.cm', '18B107EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd79', '676272498', 'etudiant', 'active'),
(77, 0, 0, '18B108EB', 'akambanicolevanessafernande@ensbertoua.cm', '18B108EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd80', '694579884', 'etudiant', 'active'),
(78, 0, 0, '18B109EB', 'ayemafonwatigervaise@ensbertoua.cm', '18B109EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd81', '688041297', 'etudiant', 'active'),
(79, 0, 0, '18B110EB', 'djoumessigoufacksylviane@ensbertoua.cm', '18B110EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd82', '688364392', 'etudiant', 'active'),
(80, 0, 0, '18B111EB', 'dountiodjassimurielle@ensbertoua.cm', '18B111EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd83', '683406196', 'etudiant', 'active'),
(81, 0, 0, '18B112EB', 'fadimatou@ensbertoua.cm', '18B112EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd84', '673616046', 'etudiant', 'active'),
(82, 0, 0, '18B113EB', 'mbonadinewilly@ensbertoua.cm', '18B113EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd85', '669430922', 'etudiant', 'active'),
(83, 0, 0, '18B114EB', 'mbougangpaulfelix@ensbertoua.cm', '18B114EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd86', '668058197', 'etudiant', 'active'),
(84, 0, 0, '18B115EB', 'ngouchekenadjara@ensbertoua.cm', '18B115EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd87', '676815063', 'etudiant', 'active'),
(85, 0, 0, '18B116EB', 'sandjongfomenijoyaniemonelle@ensbertoua.cm', '18B116EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd88', '695857033', 'etudiant', 'active'),
(86, 0, 0, '18B117EB', 'temgouazeffackchristianlauren@ensbertoua.cm', '18B117EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd89', '658703996', 'etudiant', 'active'),
(87, 0, 0, '18B118EB', 'fokouwojouombodidier@ensbertoua.cm', '18B118EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd90', '691366531', 'etudiant', 'active'),
(88, 0, 0, '18B119EB', 'kenguengouenejocelyndirane@ensbertoua.cm', '18B119EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd91', '674742568', 'etudiant', 'active'),
(89, 0, 0, '18B120EB', 'nkongaliliane@ensbertoua.cm', '18B120EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd92', '659584579', 'etudiant', 'active'),
(90, 0, 0, '18B121EB', 'noahabinidemaison@ensbertoua.cm', '18B121EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd93', '650973308', 'etudiant', 'active'),
(91, 0, 0, '18B233EB', 'obamaottoujosepholivierthier@ensbertoua.cm', '18B233EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd94', '692881302', 'etudiant', 'active'),
(92, 0, 0, '18B234EB', 'mpoedjodjollilianelaure@ensbertoua.cm', '18B234EB@ensbertoua.cm', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd95', '656879665', 'etudiant', 'active');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `anonymat`
--
ALTER TABLE `anonymat`
  ADD PRIMARY KEY (`id_anonymat`);

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id_classe`);

--
-- Index pour la table `composant_classe_virtuelle`
--
ALTER TABLE `composant_classe_virtuelle`
  ADD PRIMARY KEY (`id_classe_virtuelle`,`id_classe_physique`);

--
-- Index pour la table `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id_departement`);

--
-- Index pour la table `desinscrire_etudiant_ue`
--
ALTER TABLE `desinscrire_etudiant_ue`
  ADD PRIMARY KEY (`id_desinscription_ue`);

--
-- Index pour la table `element_constitutif`
--
ALTER TABLE `element_constitutif`
  ADD PRIMARY KEY (`id_element_constitutif`);

--
-- Index pour la table `element_grille`
--
ALTER TABLE `element_grille`
  ADD PRIMARY KEY (`id_element_grille`);

--
-- Index pour la table `element_grille_classe`
--
ALTER TABLE `element_grille_classe`
  ADD PRIMARY KEY (`id_element_grille_classe`);

--
-- Index pour la table `enseignant`
--
ALTER TABLE `enseignant`
  ADD PRIMARY KEY (`id_enseignant`);

--
-- Index pour la table `enseignant_departement`
--
ALTER TABLE `enseignant_departement`
  ADD PRIMARY KEY (`id_enseignant_departement`);

--
-- Index pour la table `etudiant`
--
ALTER TABLE `etudiant`
  ADD PRIMARY KEY (`matricule`);

--
-- Index pour la table `filiere`
--
ALTER TABLE `filiere`
  ADD PRIMARY KEY (`id_filiere`);

--
-- Index pour la table `grille`
--
ALTER TABLE `grille`
  ADD PRIMARY KEY (`id_grille`);

--
-- Index pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD PRIMARY KEY (`id_inscription`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id_log`);

--
-- Index pour la table `note_composition`
--
ALTER TABLE `note_composition`
  ADD PRIMARY KEY (`id_note_composition`);

--
-- Index pour la table `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`id_privilege`);

--
-- Index pour la table `privilege_profil`
--
ALTER TABLE `privilege_profil`
  ADD PRIMARY KEY (`id_profil`,`id_privilege`);

--
-- Index pour la table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id_profil`);

--
-- Index pour la table `profil_utilisateur`
--
ALTER TABLE `profil_utilisateur`
  ADD PRIMARY KEY (`id_profil_utilisateur`);

--
-- Index pour la table `reinscrire_etudiant_ue`
--
ALTER TABLE `reinscrire_etudiant_ue`
  ADD PRIMARY KEY (`id_reinscription_ue`);

--
-- Index pour la table `unite_enseignement`
--
ALTER TABLE `unite_enseignement`
  ADD PRIMARY KEY (`code_ue`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `anonymat`
--
ALTER TABLE `anonymat`
  MODIFY `id_anonymat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
  MODIFY `id_classe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `departement`
--
ALTER TABLE `departement`
  MODIFY `id_departement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `desinscrire_etudiant_ue`
--
ALTER TABLE `desinscrire_etudiant_ue`
  MODIFY `id_desinscription_ue` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `element_constitutif`
--
ALTER TABLE `element_constitutif`
  MODIFY `id_element_constitutif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `element_grille`
--
ALTER TABLE `element_grille`
  MODIFY `id_element_grille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `element_grille_classe`
--
ALTER TABLE `element_grille_classe`
  MODIFY `id_element_grille_classe` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `enseignant`
--
ALTER TABLE `enseignant`
  MODIFY `id_enseignant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `enseignant_departement`
--
ALTER TABLE `enseignant_departement`
  MODIFY `id_enseignant_departement` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `filiere`
--
ALTER TABLE `filiere`
  MODIFY `id_filiere` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `grille`
--
ALTER TABLE `grille`
  MODIFY `id_grille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inscription`
--
ALTER TABLE `inscription`
  MODIFY `id_inscription` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `note_composition`
--
ALTER TABLE `note_composition`
  MODIFY `id_note_composition` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT pour la table `privilege`
--
ALTER TABLE `privilege`
  MODIFY `id_privilege` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `profil_utilisateur`
--
ALTER TABLE `profil_utilisateur`
  MODIFY `id_profil_utilisateur` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `reinscrire_etudiant_ue`
--
ALTER TABLE `reinscrire_etudiant_ue`
  MODIFY `id_reinscription_ue` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
