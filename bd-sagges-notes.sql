/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  09/03/2020 14:21:49                      */
/*==============================================================*/


drop table if exists anonymat;

drop table if exists classe;

drop table if exists composant_classe_virtuelle;

drop table if exists departement;

drop table if exists desinscrire_etudiant_ue;

drop table if exists element_constitutif;

drop table if exists element_grille;

drop table if exists element_grille_classe;

drop table if exists enseignant;

drop table if exists enseignant_departement;

drop table if exists etudiant;

drop table if exists filiere;

drop table if exists grille;

drop table if exists inscription;

drop table if exists logs;

drop table if exists note_composition;

drop table if exists privilege;

drop table if exists privilege_profil;

drop table if exists profil;

drop table if exists profil_utilisateur;

drop table if exists reinscrire_etudiant_ue;

drop table if exists unite_enseignement;

drop table if exists utilisateur;

/*==============================================================*/
/* Table : anonymat                                             */
/*==============================================================*/
create table anonymat
(
   id_anonymat          int not null auto_increment,
   code_anonymat        varchar(40),
   matricule            varchar(10) not null,
   code_ue              varchar(10) not null,
   session              char(10),
   primary key (id_anonymat)
);

/*==============================================================*/
/* Table : classe                                               */
/*==============================================================*/
create table classe
(
   id_classe            int not null,
   id_filiere           int,
   nom_classe           varchar(255),
   niveau               int,
   type                 varchar(25),
   primary key (id_classe)
);

/*==============================================================*/
/* Table : composant_classe_virtuelle                           */
/*==============================================================*/
create table composant_classe_virtuelle
(
   id_classe_virtuelle  int not null,
   id_classe_physique   int not null,
   primary key (id_classe_virtuelle, id_classe_physique)
);

/*==============================================================*/
/* Table : departement                                          */
/*==============================================================*/
create table departement
(
   id_departement       int not null auto_increment,
   nom_departement      varchar(255),
   date_creation        date,
   numero_texte_creation varchar(50),
   primary key (id_departement)
);

/*==============================================================*/
/* Table : desinscrire_etudiant_ue                              */
/*==============================================================*/
create table desinscrire_etudiant_ue
(
   id_desinscription_ue int not null auto_increment,
   matricule            varchar(10) not null,
   code_ue              varchar(10) not null,
   primary key (id_desinscription_ue)
);

/*==============================================================*/
/* Table : element_constitutif                                  */
/*==============================================================*/
create table element_constitutif
(
   id_element_constitutif int not null auto_increment,
   code_ec              varchar(15),
   nom_ec               varchar(255),
   primary key (id_element_constitutif)
);

/*==============================================================*/
/* Table : element_grille                                       */
/*==============================================================*/
create table element_grille
(
   id_element_grille    int not null auto_increment,
   id_grille            int,
   id_element_constitutif int not null,
   code_ue              varchar(10) not null,
   id_enseignant        int not null,
   pourcentagetp        int,
   pourcentageprojet    int,
   pourcentagecc        int,
   pourcentageexamen    int,
   credit               int,
   semestre             int,
   annee_academique     varchar(10),
   role                 varchar(50),
   primary key (id_element_grille)
);

/*==============================================================*/
/* Table : element_grille_classe                                */
/*==============================================================*/
create table element_grille_classe
(
   id_element_grille_classe int not null auto_increment,
   id_element_grille    int not null,
   id_classe            int not null,
   primary key (id_element_grille_classe)
);

/*==============================================================*/
/* Table : enseignant                                           */
/*==============================================================*/
create table enseignant
(
   id_enseignant        int not null auto_increment,
   nom_prenom           varchar(255),
   grade                varchar(15),
   numero_cni           varchar(100),
   specialite_recherche text,
   telephone            varchar(10),
   ville_residence      varchar(25),
   categorie            varchar(255),
   sexe                 char(1),
   primary key (id_enseignant)
);

/*==============================================================*/
/* Table : enseignant_departement                               */
/*==============================================================*/
create table enseignant_departement
(
   id_enseignant_departement int not null auto_increment,
   id_enseignant        int not null,
   id_departement       int not null,
   role                 varchar(50),
   debut_fonction       date,
   fin_fonction         date,
   primary key (id_enseignant_departement)
);

/*==============================================================*/
/* Table : etudiant                                             */
/*==============================================================*/
create table etudiant
(
   matricule            varchar(10) not null,
   nom_prenom           varchar(255),
   date_naissance       date,
   lieu_naissance       varchar(254),
   sexe                 char(1),
   num_cni              varchar(40),
   region_origine       varchar(10),
   telephone            varchar(10),
   email                varchar(255),
   telephone_parent     varchar(10),
   primary key (matricule)
);

/*==============================================================*/
/* Table : filiere                                              */
/*==============================================================*/
create table filiere
(
   id_filiere           int not null auto_increment,
   id_departement       int not null,
   code_filliere        varchar(15),
   nom_filiere          varchar(250),
   cycle                varchar(15),
   date_creation        date,
   primary key (id_filiere)
);

/*==============================================================*/
/* Table : grille                                               */
/*==============================================================*/
create table grille
(
   id_grille            int not null auto_increment,
   id_classe            int not null,
   annee_academique     varchar(10),
   primary key (id_grille)
);

/*==============================================================*/
/* Table : inscription                                          */
/*==============================================================*/
create table inscription
(
   id_inscription       int not null auto_increment,
   matricule            varchar(10) not null,
   id_classe            int not null,
   annee_academique     varchar(10),
   primary key (id_inscription)
);

/*==============================================================*/
/* Table : logs                                                 */
/*==============================================================*/
create table logs
(
   id_log               int not null auto_increment,
   id_utilisateur       int,
   date_heure           datetime,
   action               text,
   primary key (id_log)
);

/*==============================================================*/
/* Table : note_composition                                     */
/*==============================================================*/
create table note_composition
(
   id_note_composition  int not null auto_increment,
   matricule            varchar(10) not null,
   id_element_grille    int not null,
   note                 decimal,
   type_composition     char(255),
   date_composition     date,
   primary key (id_note_composition)
);

/*==============================================================*/
/* Table : privilege                                            */
/*==============================================================*/
create table privilege
(
   id_privilege         int not null auto_increment,
   nom_privilege        varchar(50),
   type_privilege       varchar(255),
   description          text,
   primary key (id_privilege)
);

/*==============================================================*/
/* Table : privilege_profil                                     */
/*==============================================================*/
create table privilege_profil
(
   id_profil            int not null,
   id_privilege         int not null,
   primary key (id_profil, id_privilege)
);

/*==============================================================*/
/* Table : profil                                               */
/*==============================================================*/
create table profil
(
   id_profil            int not null auto_increment,
   nom_profil           varchar(25),
   type_profil          varchar(25),
   primary key (id_profil)
);

/*==============================================================*/
/* Table : profil_utilisateur                                   */
/*==============================================================*/
create table profil_utilisateur
(
   id_profil_utilisateur int not null auto_increment,
   id_profil            int not null,
   id_utilisateur       int,
   primary key (id_profil_utilisateur)
);

/*==============================================================*/
/* Table : reinscrire_etudiant_ue                               */
/*==============================================================*/
create table reinscrire_etudiant_ue
(
   id_reinscription_ue  int not null auto_increment,
   matricule            varchar(10) not null,
   code_ue              varchar(10) not null,
   primary key (id_reinscription_ue)
);

/*==============================================================*/
/* Table : unite_enseignement                                   */
/*==============================================================*/
create table unite_enseignement
(
   code_ue              varchar(10) not null,
   id_enseignant        int not null,
   nom_ue               varchar(255),
   type                 varchar(25),
   primary key (code_ue)
);

/*==============================================================*/
/* Table : utilisateur                                          */
/*==============================================================*/
create table utilisateur
(
   id_utilisateur       int not null auto_increment,
   id_enseignant        int,
   special_id           int,
   matricule            varchar(10),
   email                varchar(255),
   username             varchar(255),
   password             varchar(255),
   telephone            varchar(10),
   role                 varchar(50),
   etat                 varchar(10),
   primary key (id_utilisateur)
);

