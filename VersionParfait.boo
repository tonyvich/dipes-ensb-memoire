<?xml version="1.0" encoding="UTF-8"?>
<?PowerDesigner AppLocale="UTF16" ID="{26EB48CA-B609-4F43-AAA5-C630B65AF316}" Label="" LastModificationDate="1584871527" Name="DiagrammesDeClasse" Objects="139" Symbols="44" Target="Analyse" TargetLink="Reference" Type="{18112060-1A4B-11D1-83D9-444553540000}" signature="CLD_OBJECT_MODEL" version="15.1.0.2850"?>
<!-- Veuillez ne pas modifier ce fichier -->

<Model xmlns:a="attribute" xmlns:c="collection" xmlns:o="object">

<o:RootObject Id="o1">
<c:Children>
<o:Model Id="o2">
<a:ObjectID>26EB48CA-B609-4F43-AAA5-C630B65AF316</a:ObjectID>
<a:Name>DiagrammesDeClasse</a:Name>
<a:Code>DIAGRAMMESDECLASSE</a:Code>
<a:CreationDate>1584626662</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {5EF6F072-FC6E-41E7-AAC2-2B77AC4ECDFA}
DAT 1584626688
ATT </a:History>
<a:PackageOptionsText>[FolderOptions]

[FolderOptions\Class Diagram Objects]
GenerationCheckModel=Yes
GenerationPath=
GenerationOptions=
GenerationTasks=
GenerationTargets=
GenerationSelections=</a:PackageOptionsText>
<a:ModelOptionsText>[ModelOptions]

[ModelOptions\Cld]
CaseSensitive=No
DisplayName=Yes
EnableTrans=Yes
EnableRequirements=No
ShowClss=No
DeftAttr=int
DeftMthd=int
DeftParm=int
DeftCont=
DomnDttp=Yes
DomnChck=No
DomnRule=No
SupportDelay=No
PreviewEditable=Yes
AutoRealize=No
DttpFullName=Yes
DeftClssAttrVisi=private
VBNetPreprocessingSymbols=
CSharpPreprocessingSymbols=

[ModelOptions\Cld\NamingOptionsTemplates]

[ModelOptions\Cld\ClssNamingOptions]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG]

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPCKG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN]

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDDOMN\Code]
Template=
MaxLen=254
Case=U
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS]

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDCLASS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF]

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDINTF\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR]

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDACTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS]

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDUCAS\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT]

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDOBJT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG]

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\SQDMSSG\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP]

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,FirstUpperChar)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CPDCOMP\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR]

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDATTR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD]

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDMETHOD\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM]

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDPARM\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT]

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPORT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART]

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMPART\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC]

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,,,firstLowerWord)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\CLDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC]

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\UCDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK]

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\GNRLLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK]

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RQLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK]

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\RLZSLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK]

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DEPDLINK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV]

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMACTV\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST]

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\ACDOBST\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT]

[ModelOptions\Cld\ClssNamingOptions\STAT\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\STAT\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE]

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDNODE\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI]

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDCMPI\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC]

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DPDASSC\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR]

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\OOMVAR\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO]

[ModelOptions\Cld\ClssNamingOptions\FILO\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=&quot;\/:*?&lt;&gt;|&quot;
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FILO\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_. &quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ]

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMEOBJ\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK]

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\FRMELNK\Code]
Template=
MaxLen=254
Case=M
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass]

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Name]
Template=
MaxLen=254
Case=M
ValidChar=
InvldChar=
AllValid=Yes
NoAccent=No
DefaultChar=_
Script=.convert_name(%Name%,&quot;_&quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Cld\ClssNamingOptions\DefaultClass\Code]
Template=
MaxLen=254
Case=U
ValidChar=&#39;a&#39;-&#39;z&#39;,&#39;A&#39;-&#39;Z&#39;,&#39;0&#39;-&#39;9&#39;,&quot;_&quot;
InvldChar=&quot; +-*/!=&lt;&gt;&#39;&quot;&quot;().&quot;
AllValid=Yes
NoAccent=Yes
DefaultChar=_
Script=.convert_code(%Code%,&quot; &quot;)
ConvTable=
ConvTablePath=%_HOME%\Fichiers de ressources\Tables de conversion

[ModelOptions\Generate]

[ModelOptions\Generate\Cdm]
CheckModel=Yes
SaveLinks=Yes
NameToCode=No
Notation=2

[ModelOptions\Generate\Pdm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No
BuildTrgr=No
TablePrefix=
RefrUpdRule=RESTRICT
RefrDelRule=RESTRICT
IndxPKName=%TABLE%_PK
IndxAKName=%TABLE%_AK
IndxFKName=%REFR%_FK
IndxThreshold=
ColnFKName=%.3:PARENT%_%COLUMN%
ColnFKNameUse=No

[ModelOptions\Generate\Xsm]
CheckModel=Yes
SaveLinks=Yes
ORMapping=No
NameToCode=No</a:ModelOptionsText>
<c:GenerationOrigins>
<o:Shortcut Id="o3">
<a:ObjectID>0A2B02E0-450F-4660-BF18-877706EAB076</a:ObjectID>
<a:Name>Données conceptuelles_1</a:Name>
<a:Code>DONNEES_CONCEPTUELLES_1</a:Code>
<a:CreationDate>1584626689</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626689</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>5EF6F072-FC6E-41E7-AAC2-2B77AC4ECDFA</a:TargetID>
<a:TargetClassID>1E597170-9350-11D1-AB3C-0020AF71E433</a:TargetClassID>
</o:Shortcut>
</c:GenerationOrigins>
<c:ObjectLanguage>
<o:Shortcut Id="o4">
<a:ObjectID>17CEB94E-0FE9-47E4-B6C6-43D024E5B714</a:ObjectID>
<a:Name>Analyse</a:Name>
<a:Code>Analysis</a:Code>
<a:CreationDate>1584626674</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626674</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:TargetStereotype/>
<a:TargetID>87317290-AF03-469F-BC1E-99593F18A4AB</a:TargetID>
<a:TargetClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetClassID>
</o:Shortcut>
</c:ObjectLanguage>
<c:ClassDiagrams>
<o:ClassDiagram Id="o5">
<a:ObjectID>6B65428F-4709-4239-ABB7-AA5828ADF31D</a:ObjectID>
<a:Name>Diagramme_1</a:Name>
<a:Code>DIAGRAMME_1</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {7BB49EB4-BD6B-4DAD-B0AB-6DD40BCE15CD}
DAT 1584626688</a:History>
<a:DisplayPreferences>[DisplayPreferences]

[DisplayPreferences\CLD]

[DisplayPreferences\General]
Adjust to text=Yes
Snap Grid=No
Constrain Labels=Yes
Display Grid=No
Show Page Delimiter=Yes
Grid size=0
Graphic unit=2
Window color=255, 255, 255
Background image=
Background mode=8
Watermark image=
Watermark mode=8
Show watermark on screen=No
Gradient mode=0
Gradient end color=255, 255, 255
Show Swimlane=No
SwimlaneVert=Yes
TreeVert=No
CompDark=0

[DisplayPreferences\Object]
Mode=2
Trunc Length=40
Word Length=40
Word Text=!&quot;&quot;#$%&amp;&#39;()*+,-./:;&lt;=&gt;?@[\]^_`{|}~
Shortcut IntIcon=Yes
Shortcut IntLoct=Yes
Shortcut IntFullPath=No
Shortcut IntLastPackage=Yes
Shortcut ExtIcon=Yes
Shortcut ExtLoct=No
Shortcut ExtFullPath=No
Shortcut ExtLastPackage=Yes
Shortcut ExtIncludeModl=Yes
EObjShowStrn=Yes
ExtendedObject.Comment=No
ExtendedObject.IconPicture=No
ExtendedObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Nom de l&amp;#39;objet&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF] &lt;Separator Name=&quot;Séparateur&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Commentaire&quot; Attribute=&quot;Comment&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;LEFT&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
ELnkShowStrn=Yes
ELnkShowName=Yes
ExtendedLink_SymbolLayout=&lt;Form&gt;[CRLF] &lt;Form Name=&quot;Centre&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Source&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF] &lt;Form Name=&quot;Destination&quot; &gt;[CRLF] &lt;/Form&gt;[CRLF]&lt;/Form&gt;
FileObject.Stereotype=No
FileObject.DisplayName=Yes
FileObject.LocationOrName=No
FileObject.IconPicture=No
FileObject.IconMode=Yes
FileObject_SymbolLayout=&lt;Form&gt;[CRLF] &lt;StandardAttribute Name=&quot;Stéréotype&quot; Attribute=&quot;Stereotype&quot; Prefix=&quot;&amp;lt;&amp;lt;&quot; Suffix=&quot;&amp;gt;&amp;gt;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;ExclusiveChoice Name=&quot;Choix exclusif&quot; Mandatory=&quot;Yes&quot; Display=&quot;HorizontalRadios&quot; &gt;[CRLF]  &lt;StandardAttribute Name=&quot;Nom&quot; Attribute=&quot;DisplayName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF]  &lt;StandardAttribute Name=&quot;Emplacement&quot; Attribute=&quot;LocationOrName&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;No&quot; /&gt;[CRLF] &lt;/ExclusiveChoice&gt;[CRLF] &lt;StandardAttribute Name=&quot;Icône&quot; Attribute=&quot;IconPicture&quot; Prefix=&quot;&quot; Suffix=&quot;&quot; Alignment=&quot;CNTR&quot; Caption=&quot;&quot; Mandatory=&quot;Yes&quot; /&gt;[CRLF]&lt;/Form&gt;
PckgShowStrn=Yes
Package.Comment=No
Package.IconPicture=No
Package_SymbolLayout=
Display Model Version=Yes
Class.IconPicture=No
Class_SymbolLayout=
Interface.IconPicture=No
Interface_SymbolLayout=
Port.IconPicture=No
Port_SymbolLayout=
ClssShowSttr=Yes
Class.Comment=No
ClssShowCntr=Yes
ClssShowAttr=Yes
ClssAttrTrun=No
ClssAttrMax=3
ClssShowMthd=Yes
ClssMthdTrun=No
ClssMthdMax=3
ClssShowInnr=Yes
IntfShowSttr=Yes
Interface.Comment=No
IntfShowAttr=Yes
IntfAttrTrun=No
IntfAttrMax=3
IntfShowMthd=Yes
IntfMthdTrun=No
IntfMthdMax=3
IntfShowCntr=Yes
IntfShowInnr=Yes
PortShowName=Yes
PortShowType=No
PortShowMult=No
AttrShowVisi=Yes
AttrVisiFmt=1
AttrShowStrn=Yes
AttrShowDttp=Yes
AttrShowDomn=No
AttrShowInit=Yes
MthdShowVisi=Yes
MthdVisiFmt=1
MthdShowStrn=Yes
MthdShowRttp=Yes
MthdShowParm=Yes
AsscShowName=No
AsscShowCntr=Yes
AsscShowRole=Yes
AsscShowOrdr=Yes
AsscShowMult=Yes
AsscMultStr=Yes
AsscShowStrn=No
GnrlShowName=No
GnrlShowStrn=Yes
GnrlShowCntr=Yes
RlzsShowName=No
RlzsShowStrn=Yes
RlzsShowCntr=Yes
DepdShowName=No
DepdShowStrn=Yes
DepdShowCntr=Yes
RqlkShowName=No
RqlkShowStrn=Yes
RqlkShowCntr=Yes
PckgStrn=Yes
EnttAttr=Yes
PentMode=0
PentNb=5
EnttDttp=Yes
EnttDomn=Yes
EnttShowDomn=No
EnttMand=Yes
EnttCIdf=Yes
EnttKeyI=Yes
PentStrn=Yes
IdtfStrn=Yes
RlshName=Yes
RlshRole=Yes
RlshCard=No
RlshDmnt=Yes
RlshStrn=Yes
InhrName=Yes
InhrStrn=Yes
Entity.IconPicture=No
Entity_SymbolLayout=
Association.IconPicture=No
Association_SymbolLayout=
Inheritance.IconPicture=No
Inheritance_SymbolLayout=
EnttStrn=Yes
EnttCmmt=No
AsscStrn=Yes
AsscCmmt=No
AsscAttr=Yes
AsscDttp=Yes
AsscDomn=Yes
AsscShowDomn=No
AsscMand=Yes
AsscDLim=Yes
AsscNb=5
PassStrn=1
LinkRole=Yes
LinkCard=Yes
LinkStrn=Yes

[DisplayPreferences\Symbol]

[DisplayPreferences\Symbol\FRMEOBJ]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=6000
Height=2000
Brush color=255 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=64
Brush gradient color=192 192 192
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 255 128 128
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FRMELNK]
CENTERFont=Arial,8,N
CENTERFont color=0, 0, 0
Line style=1
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\FILO]
OBJSTRNFont=Arial,8,N
OBJSTRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LCNMFont=Arial,8,N
LCNMFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDPCKG]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDCLASS]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
CNTRFont=Arial,8,N
CNTRFont color=0, 0, 0
AttributesFont=Arial,8,N
AttributesFont color=0, 0, 0
ClassPrimaryAttributeFont=Arial,8,U
ClassPrimaryAttributeFont color=0, 0, 0
OperationsFont=Arial,8,N
OperationsFont color=0, 0, 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=174 228 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDINTF]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
CNTRFont=Arial,8,N
CNTRFont color=0, 0, 0
AttributesFont=Arial,8,N
AttributesFont color=0, 0, 0
OperationsFont=Arial,8,N
OperationsFont color=0, 0, 0
InnerClassifiersFont=Arial,8,N
InnerClassifiersFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=208 208 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\OOMPORT]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=800
Height=800
Brush color=174 228 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDASSC]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
MULAFont=Arial,8,N
MULAFont color=0, 0, 0
Line style=1
Pen=1 0 0 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\INNERLINK]
Line style=1
Pen=1 0 0 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CLDACLK]
Line style=1
Pen=2 0 0 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\GNRLLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
Line style=1
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLZSLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
Line style=1
Pen=3 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RQLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
Line style=1
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\DEPDLINK]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
Line style=1
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\USRDEPD]
OBJXSTRFont=Arial,8,N
OBJXSTRFont color=0, 0, 0
Line style=1
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=2 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\Free Symbol]
Free TextFont=Arial,8,N
Free TextFont color=0, 0, 0
Line style=0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 0 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CDMPCKG]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3600
Brush color=255 255 192
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 178 178 178
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ENTT]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
AttributesFont=Arial,8,N
AttributesFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
EntityPrimaryAttributeFont=Arial,8,U
EntityPrimaryAttributeFont color=0, 0, 0
IdentifiersFont=Arial,8,N
IdentifiersFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=4000
Brush color=176 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 170 170
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\RLSH]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
NAMAFont=Arial,8,N
NAMAFont color=0, 0, 0
Line style=1
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Brush color=255 255 255
Fill Color=Yes
Brush style=1
Brush bitmap mode=12
Brush gradient mode=0
Brush gradient color=118 118 118
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 0 170 170
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\ASSC]
STRNFont=Arial,8,N
STRNFont color=0, 0, 0
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
AttributesFont=Arial,8,N
AttributesFont color=0, 0, 0
LABLFont=Arial,8,N
LABLFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=No
Width=4800
Height=3000
Brush color=208 208 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\LINK]
ROLEFont=Arial,8,N
ROLEFont color=0, 0, 0
CARDFont=Arial,8,N
CARDFont color=0, 0, 0
Line style=1
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\CDMINHR]
DISPNAMEFont=Arial,8,N
DISPNAMEFont color=0, 0, 0
AutoAdjustToText=Yes
Keep aspect=No
Keep center=No
Keep size=Yes
Width=1600
Height=1000
Brush color=176 255 255
Fill Color=Yes
Brush style=6
Brush bitmap mode=12
Brush gradient mode=65
Brush gradient color=255 255 255
Brush background image=
Custom shape=
Custom text mode=0
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\Symbol\LINH]
Line style=1
Pen=1 0 128 128 255
Shadow color=192 192 192
Shadow=0

[DisplayPreferences\CDM]</a:DisplayPreferences>
<a:PaperSize>(8268, 11693)</a:PaperSize>
<a:PageMargins>((315,354), (433,354))</a:PageMargins>
<a:PageOrientation>1</a:PageOrientation>
<a:PaperSource>15</a:PaperSource>
<c:Symbols>
<o:AssociationSymbol Id="o6">
<a:Rect>((3488,22680), (38476,29199))</a:Rect>
<a:ListOfPoints>((3525,29199),(3525,23875),(38476,23875))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o7"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o8"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o9"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o10">
<a:CreationDate>1584626688</a:CreationDate>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Rect>((3525,23875), (32517,33309))</a:Rect>
<a:ListOfPoints>((3525,23875),(3525,33309),(32517,33309))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744448</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o6"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o11"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o12"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:AssociationSymbol Id="o13">
<a:Rect>((-10212,-508), (15414,6930))</a:Rect>
<a:ListOfPoints>((-10175,6930),(-10175,666),(15414,666))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o15"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o16"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o17">
<a:Rect>((-10025,7080), (11251,14123))</a:Rect>
<a:ListOfPoints>((9414,14123),(9414,7080),(-10025,7080))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o18"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o19"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o20">
<a:Rect>((13727,816), (27146,11574))</a:Rect>
<a:ListOfPoints>((15564,816),(15564,11574),(27146,11574))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o15"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o21"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o22"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o23">
<a:Rect>((-34834,16096), (-26250,47222))</a:Rect>
<a:ListOfPoints>((-34834,46048),(-27796,46048),(-27796,16096))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o24"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o25"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o26"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o27">
<a:Rect>((-10800,-14488), (10753,-6902))</a:Rect>
<a:ListOfPoints>((10716,-14488),(10716,-8077),(-10800,-8077))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o28"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o29"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o30"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o31">
<a:Rect>((-27796,16096), (5549,29349))</a:Rect>
<a:ListOfPoints>((3675,29349),(3675,16096),(-27796,16096))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o7"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o25"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o32"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o33">
<a:CreationDate>1584626688</a:CreationDate>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Rect>((-14025,16096), (3675,29375))</a:Rect>
<a:ListOfPoints>((3675,16096),(3675,29375),(-14025,29375))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744448</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o31"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o34"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o35"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:AssociationSymbol Id="o36">
<a:Rect>((-2059,29499), (3825,46772))</a:Rect>
<a:ListOfPoints>((3825,29499),(3825,39203),(-2059,39203),(-2059,46772))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o7"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o37"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o38"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o39">
<a:Rect>((2138,29649), (19501,43374))</a:Rect>
<a:ListOfPoints>((3975,29649),(3975,43374),(19501,43374))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o7"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o40"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o41"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o42">
<a:Rect>((4125,14273), (9564,29799))</a:Rect>
<a:ListOfPoints>((4125,29799),(4125,20200),(9564,20200),(9564,14273))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o7"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o18"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o43"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o44">
<a:Rect>((-34684,45311), (-2059,47659))</a:Rect>
<a:ListOfPoints>((-34684,46485),(-2059,46485))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o24"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o37"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o45"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o46">
<a:Rect>((-27796,7230), (-9838,17270))</a:Rect>
<a:ListOfPoints>((-9875,7230),(-9875,16096),(-27796,16096))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o25"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o47"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o48">
<a:CreationDate>1584626688</a:CreationDate>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Rect>((-26887,5863), (-9875,16096))</a:Rect>
<a:ListOfPoints>((-9875,16096),(-9875,5863),(-26887,5863))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744448</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o46"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o49"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o50"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:AssociationSymbol Id="o51">
<a:Rect>((-32137,-18052), (-10800,-8076))</a:Rect>
<a:ListOfPoints>((-30300,-18052),(-30300,-8077),(-10800,-8077))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o52"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o29"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o53"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o54">
<a:Rect>((27296,11724), (40313,23875))</a:Rect>
<a:ListOfPoints>((27296,11724),(38476,11724),(38476,23875))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o21"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o8"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o55"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationClassLinkSymbol Id="o56">
<a:CreationDate>1584626688</a:CreationDate>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Rect>((20137,11724), (38476,20311))</a:Rect>
<a:ListOfPoints>((38476,11724),(38476,20311),(20137,20311))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744448</a:LineColor>
<a:DashStyle>2</a:DashStyle>
<a:ShadowColor>12632256</a:ShadowColor>
<c:SourceSymbol>
<o:AssociationSymbol Ref="o54"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o57"/>
</c:DestinationSymbol>
<c:Object>
<o:AssociationClassLink Ref="o58"/>
</c:Object>
</o:AssociationClassLinkSymbol>
<o:AssociationSymbol Id="o59">
<a:Rect>((-30150,-23301), (-3600,-16727))</a:Rect>
<a:ListOfPoints>((-3600,-23301),(-3600,-17902),(-30150,-17902))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o60"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o52"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o61"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o62">
<a:Rect>((-29670,16096), (-1909,46922))</a:Rect>
<a:ListOfPoints>((-1909,46922),(-27796,46922),(-27796,16096))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o37"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o25"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o63"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o64">
<a:Rect>((-29520,16246), (-1759,47072))</a:Rect>
<a:ListOfPoints>((-1759,47072),(-27646,47072),(-27646,16246))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o37"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o25"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o65"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o66">
<a:Rect>((-1609,22701), (38476,47222))</a:Rect>
<a:ListOfPoints>((-1609,47222),(-1609,23875),(38476,23875))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>2048</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o37"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o8"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o67"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o68">
<a:Rect>((-11862,7080), (-2440,12675))</a:Rect>
<a:ListOfPoints>((-9725,7380),(-3677,7380),(-3677,12101),(-10025,12101),(-10025,7080))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o14"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o69"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o70">
<a:Rect>((-9575,7530), (6149,29949))</a:Rect>
<a:ListOfPoints>((-9575,7530),(4275,7530),(4275,29949))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>0</a:ArrowStyle>
<a:LineColor>16744576</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o14"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o7"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o71"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o72">
<a:Rect>((-32025,-9250), (-10800,14126))</a:Rect>
<a:ListOfPoints>((-32025,12952),(-18506,12952),(-18506,-8077),(-10800,-8077))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>11184640</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o25"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o29"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o73"/>
</c:Object>
</o:AssociationSymbol>
<o:AssociationSymbol Id="o74">
<a:Rect>((-9016,-9276), (37127,18636))</a:Rect>
<a:ListOfPoints>((35140,18636),(35140,-9276),(-9016,-9276))</a:ListOfPoints>
<a:CornerStyle>1</a:CornerStyle>
<a:ArrowStyle>8</a:ArrowStyle>
<a:LineColor>11184640</a:LineColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>DISPNAME 0 Arial,8,N
MULA 0 Arial,8,N</a:FontList>
<c:SourceSymbol>
<o:ClassSymbol Ref="o8"/>
</c:SourceSymbol>
<c:DestinationSymbol>
<o:ClassSymbol Ref="o29"/>
</c:DestinationSymbol>
<c:Object>
<o:Association Ref="o75"/>
</c:Object>
</o:AssociationSymbol>
<o:ClassSymbol Id="o29">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-15228,-12422), (-6372,-3732))</a:Rect>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o76"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o25">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-33808,9803), (-21784,22389))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o77"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o28">
<a:IconMode>-1</a:IconMode>
<a:Rect>((6056,-17372), (15376,-11604))</a:Rect>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o78"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o24">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-40344,43164), (-29324,48932))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o79"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o37">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-6217,43888), (2099,49656))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o80"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o18">
<a:IconMode>-1</a:IconMode>
<a:Rect>((3171,11726), (15657,16520))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o81"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o21">
<a:IconMode>-1</a:IconMode>
<a:Rect>((20362,8203), (33930,14945))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o82"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o15">
<a:IconMode>-1</a:IconMode>
<a:Rect>((10329,-3192), (20499,4524))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o83"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o14">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-14873,3559), (-5477,10301))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o84"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o8">
<a:IconMode>-1</a:IconMode>
<a:Rect>((32155,18069), (44797,29681))</a:Rect>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o85"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o11">
<a:IconMode>-1</a:IconMode>
<a:Rect>((29248,31399), (35786,35219))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16744576</a:LineColor>
<a:FillColor>16765136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o86"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o7">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-2834,23880), (9884,34518))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o87"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o34">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-20036,26491), (-8014,32259))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16744576</a:LineColor>
<a:FillColor>16765136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o88"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o40">
<a:IconMode>-1</a:IconMode>
<a:Rect>((12871,40490), (26131,46258))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o89"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o49">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-33130,3953), (-20644,7773))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16744576</a:LineColor>
<a:FillColor>16765136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o90"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o52">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-34844,-20936), (-25756,-15168))</a:Rect>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o91"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o57">
<a:IconMode>-1</a:IconMode>
<a:Rect>((14821,17427), (25453,23195))</a:Rect>
<a:AutoAdjustToText>0</a:AutoAdjustToText>
<a:LineColor>16744576</a:LineColor>
<a:FillColor>16765136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o92"/>
</c:Object>
</o:ClassSymbol>
<o:ClassSymbol Id="o60">
<a:IconMode>-1</a:IconMode>
<a:Rect>((-8801,-26672), (1601,-19930))</a:Rect>
<a:LineColor>11184640</a:LineColor>
<a:FillColor>16777136</a:FillColor>
<a:ShadowColor>12632256</a:ShadowColor>
<a:FontList>STRN 0 Arial,8,N
DISPNAME 0 Arial,8,N
CNTR 0 Arial,8,N
Attributes 0 Arial,8,N
ClassPrimaryAttribute 0 Arial,8,U
Operations 0 Arial,8,N
InnerClassifiers 0 Arial,8,N
LABL 0 Arial,8,N</a:FontList>
<a:BrushStyle>6</a:BrushStyle>
<a:GradientFillMode>65</a:GradientFillMode>
<a:GradientEndColor>16777215</a:GradientEndColor>
<c:Object>
<o:Class Ref="o93"/>
</c:Object>
</o:ClassSymbol>
</c:Symbols>
</o:ClassDiagram>
</c:ClassDiagrams>
<c:DefaultDiagram>
<o:ClassDiagram Ref="o5"/>
</c:DefaultDiagram>
<c:Classes>
<o:Class Id="o76">
<a:ObjectID>4CE69859-0FDB-45F3-8805-6D5B90D2AC91</a:ObjectID>
<a:Name>Utilisateur</a:Name>
<a:Code>Utilisateur</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {07BE5180-CD6B-472C-8D34-1552652C0ECA}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>UTILISATEUR</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o94">
<a:ObjectID>5C74D0A5-0835-4132-8885-C135FF0B5377</a:ObjectID>
<a:Name>email</a:Name>
<a:Code>email</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F62B5EB5-CCD4-43F1-9538-F9CFABAD6693}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>EMAIL</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o95">
<a:ObjectID>1D84A994-667F-4347-9D3B-046F5D0E5724</a:ObjectID>
<a:Name>username</a:Name>
<a:Code>username</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E93C583E-91BD-4D96-B82D-447EE4FB3029}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>USERNAME</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o96">
<a:ObjectID>D2CD2C95-6618-4667-A1D0-C06BD9C0DBF1</a:ObjectID>
<a:Name>password</a:Name>
<a:Code>password</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {908021B7-BCB8-4D64-96D2-C8E2227FF0C8}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>PASSWORD</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o97">
<a:ObjectID>4B406C59-010A-4F60-87E0-ECFA9F07C6E3</a:ObjectID>
<a:Name>telephone</a:Name>
<a:Code>telephone</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {AF47F900-3B99-4BD6-B529-40A783FB3BEA}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TELEPHONE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o98">
<a:ObjectID>F82C8798-1995-4E48-9A1C-B62196DA5967</a:ObjectID>
<a:Name>role</a:Name>
<a:Code>role</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D0EC3C06-510A-49D4-BFBC-662AF190F4DB}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ROLE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o99">
<a:ObjectID>F5512A87-115E-48A1-9D98-A988232CB1B5</a:ObjectID>
<a:Name>etat</a:Name>
<a:Code>etat</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {83A9D87C-E826-4C2E-AD23-400229AE6FE3}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ETAT</a:PersistentCode>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o77">
<a:ObjectID>ECC19A12-C1E7-411E-BE73-045D12AAE128</a:ObjectID>
<a:Name>Etudiant</a:Name>
<a:Code>Etudiant</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {0D56FF55-D665-4C36-8418-3B0DFE981400}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>ETUDIANT</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o100">
<a:ObjectID>9B0662D4-C087-4797-AED7-2344748DE20A</a:ObjectID>
<a:Name>matricule</a:Name>
<a:Code>matricule</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {1982AA6E-68A7-4D31-93EF-62A9E59B13A3}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>MATRICULE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o101">
<a:ObjectID>C97C3957-FD40-4061-830C-B4F7CF085535</a:ObjectID>
<a:Name>nom_prenom</a:Name>
<a:Code>nomPrenom</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {B193E5EA-CBC2-48D6-8FB6-D053CA89A9DB}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_PRENOM</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o102">
<a:ObjectID>705BDFD6-48EC-4E46-BE85-CA165BA0E35E</a:ObjectID>
<a:Name>date_naissance</a:Name>
<a:Code>dateNaissance</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {B34D155C-B855-4C08-BAC8-8465B7291E2C}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>DATE_NAISSANCE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o103">
<a:ObjectID>1CB6CBC2-E2DB-40E6-BE8E-1BA94414FD95</a:ObjectID>
<a:Name>lieu_naissance</a:Name>
<a:Code>lieuNaissance</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {EEC9ACD0-4474-43A0-886C-0BD27282FFBB}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>LIEU_NAISSANCE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o104">
<a:ObjectID>73950CA1-1A2E-45C5-827C-AF1852A335BA</a:ObjectID>
<a:Name>sexe</a:Name>
<a:Code>sexe</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {76F467C6-777E-4ADF-A2C2-D7398560A687}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>SEXE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o105">
<a:ObjectID>581FB5DB-0206-431F-9E39-52A343F47EBA</a:ObjectID>
<a:Name>num_cni</a:Name>
<a:Code>numCni</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {C2D16E32-85F7-41D0-8BA0-D7A82BCDEE75}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NUM_CNI</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o106">
<a:ObjectID>BE352F5C-FC1E-4F32-B214-3ECE1E02A3F9</a:ObjectID>
<a:Name>region_origine</a:Name>
<a:Code>regionOrigine</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {C14490BB-3A80-451E-8D66-8F7F07F644C1}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>REGION_ORIGINE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o107">
<a:ObjectID>6A4DA3A5-4BD6-4914-A136-89668EB3ECCD</a:ObjectID>
<a:Name>telephone</a:Name>
<a:Code>telephone</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {4D58EED7-F742-4910-AC9F-1B5C2B6AA66F}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TELEPHONE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o108">
<a:ObjectID>24893AB4-6545-4DB2-8804-36EA9DF1232C</a:ObjectID>
<a:Name>email</a:Name>
<a:Code>email</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {AF6639F7-1F26-4BA1-9352-A2AD23429488}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>EMAIL</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o109">
<a:ObjectID>A30DE6AD-F84F-468A-92F9-C57A78A267CD</a:ObjectID>
<a:Name>telephone_parent</a:Name>
<a:Code>telephoneParent</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {52888FEF-56C2-4CC9-B0F0-9AA35131D30B}
DAT 1584626688</a:History>
<a:DataType>double</a:DataType>
<a:PersistentCode>TELEPHONE_PARENT</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o110">
<a:ObjectID>A01875B0-4B98-4C85-94E7-B5B75E6E6133</a:ObjectID>
<a:Name>matricule</a:Name>
<a:Code>MATRICULE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {B77E1F74-C9C0-4138-8355-3F14130530E4}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o100"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o110"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o78">
<a:ObjectID>84BEC44D-6A34-4D13-B453-F12B749842A2</a:ObjectID>
<a:Name>Logs</a:Name>
<a:Code>Logs</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {ED21F572-0325-4C50-80C1-26A25ECF3105}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>LOGS</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o111">
<a:ObjectID>5C76D47C-F090-4097-9B59-D901397A08B8</a:ObjectID>
<a:Name>id_log</a:Name>
<a:Code>idLog</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {23DF9C53-23AF-4207-A203-33DE405882FD}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_LOG</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o112">
<a:ObjectID>B023BEF5-593A-4E07-8330-C5BC9F656003</a:ObjectID>
<a:Name>date_heure</a:Name>
<a:Code>dateHeure</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {2362ACDE-86A0-4959-841E-B7E6A657A30D}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>DATE_HEURE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o113">
<a:ObjectID>F4CA5752-CA56-4092-8C4D-0E06DAF94EE6</a:ObjectID>
<a:Name>action</a:Name>
<a:Code>action</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {94EB3D2E-414B-46F7-AC54-EB4942FD36C4}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ACTION</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o114">
<a:ObjectID>F86E332E-9597-4B94-8E42-98C68B3CB19C</a:ObjectID>
<a:Name>id_utilisateur</a:Name>
<a:Code>ID_UTILISATEUR</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {839F5455-3BDE-4C28-A2D4-341BDDE4FE6E}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o111"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o114"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o79">
<a:ObjectID>6C2971C1-68A5-4EB9-9CB7-265B02ACD108</a:ObjectID>
<a:Name>Anonymat</a:Name>
<a:Code>Anonymat</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871527</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {1E8BFF96-2175-4538-B4F7-F62B4CDA6AA2}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>ANONYMAT</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o115">
<a:ObjectID>0001B95A-A1E4-426C-B099-DB1E27F08B69</a:ObjectID>
<a:Name>id_anonymat</a:Name>
<a:Code>idAnonymat</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871527</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {74CD2658-CF4E-4037-A7B1-D3E60B6E0379}
DAT 1584626688
ATT VISI</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_ANONYMAT</a:PersistentCode>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o116">
<a:ObjectID>51DF62A4-CD81-4560-AA9C-2146CA194971</a:ObjectID>
<a:Name>code_anonymat</a:Name>
<a:Code>codeAnonymat</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871527</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {680B4932-5243-4599-9081-1F0BFAE502AA}
DAT 1584626688
ATT VISI</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>CODE_ANONYMAT</a:PersistentCode>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
<o:Attribute Id="o117">
<a:ObjectID>3BCC30E1-1EA9-4278-ACAF-F35E5518CAB2</a:ObjectID>
<a:Name>session</a:Name>
<a:Code>session</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871527</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {90D7988C-BB6E-4958-8DD4-DCB4D1A5619A}
DAT 1584626688
ATT VISI</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>SESSION</a:PersistentCode>
<a:Attribute.Visibility>-</a:Attribute.Visibility>
</o:Attribute>
</c:Attributes>
<c:Operations>
<o:Operation Id="o118">
<a:ObjectID>57916BB9-1B30-4D25-A2E0-4288233CC3BD</a:ObjectID>
<a:Name>getSession</a:Name>
<a:Code>getSession</a:Code>
<a:CreationDate>1584871489</a:CreationDate>
<a:Creator>#SoftGraphic</a:Creator>
<a:ModificationDate>1584871527</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:ReturnType>String</a:ReturnType>
<a:TemplateBody>%DefaultBody%</a:TemplateBody>
</o:Operation>
</c:Operations>
<c:Identifiers>
<o:Identifier Id="o119">
<a:ObjectID>04054649-7976-4685-ACF9-C72283696A55</a:ObjectID>
<a:Name>id_anonymat</a:Name>
<a:Code>ID_ANONYMAT</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {B8A4958D-9DFF-46D5-B795-1FFD726E3FD8}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o115"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o119"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o80">
<a:ObjectID>E57846C4-DC30-4BD7-BDB0-79594D2A9B07</a:ObjectID>
<a:Name>UniteEnseignement</a:Name>
<a:Code>UniteEnseignement</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {04DA2E3B-2345-4D44-977C-2F0279B567A9}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>UNITEENSEIGNEMENT</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o120">
<a:ObjectID>5E7C8DDF-DD8C-406F-92E9-E419D5C21482</a:ObjectID>
<a:Name>code_ue</a:Name>
<a:Code>codeUe</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {9D9D8254-ED5B-4815-A4E5-0B18D632AD58}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>CODE_UE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o121">
<a:ObjectID>0C2CCBC6-1CCC-406F-90BC-6E7B914CEDAE</a:ObjectID>
<a:Name>nom_ue</a:Name>
<a:Code>nomUe</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {627C8553-05B6-42FF-B5C3-D0878B091B89}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_UE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o122">
<a:ObjectID>F0BEAC93-465E-4FB8-8051-7EB77F89DD85</a:ObjectID>
<a:Name>type</a:Name>
<a:Code>type</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {BE64ECA9-C315-4B73-8933-7767D7E193C0}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TYPE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o123">
<a:ObjectID>1B905BE8-C18B-4DD7-89C1-E91D67471795</a:ObjectID>
<a:Name>code_ue</a:Name>
<a:Code>CODE_UE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {3CA4EA0B-C1AB-4E4F-B3F5-10080A6F5A88}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o120"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o123"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o81">
<a:ObjectID>68D79B56-F433-4B6B-B0AF-33DCCCCE59C6</a:ObjectID>
<a:Name>Grille</a:Name>
<a:Code>Grille</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D68F294D-859F-4912-9420-675AC37D5EDF}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>GRILLE</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o124">
<a:ObjectID>47B3E594-3D71-484E-97CC-EA9798EF7B25</a:ObjectID>
<a:Name>id_grille</a:Name>
<a:Code>idGrille</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {0E3C649B-7AC1-4AE6-9E1B-22CDCE898649}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_GRILLE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o125">
<a:ObjectID>5CFEC6D2-BD45-4CBB-8806-467D9D0B786B</a:ObjectID>
<a:Name>annee_academique</a:Name>
<a:Code>anneeAcademique</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F9776AF0-22CE-4B36-9594-A69730512A50}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ANNEE_ACADEMIQUE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o126">
<a:ObjectID>A153F32A-5124-42CD-A09A-17C7B0B2F427</a:ObjectID>
<a:Name>id_grille</a:Name>
<a:Code>ID_GRILLE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {934E1743-36AD-464B-9B18-151B61FC4671}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o124"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o126"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o82">
<a:ObjectID>1720ADC9-DCAD-4CAE-B84B-F5B09905618C</a:ObjectID>
<a:Name>Departement</a:Name>
<a:Code>Departement</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {26018D54-2826-4465-859A-7CBB448507AE}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>DEPARTEMENT</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o127">
<a:ObjectID>C58B416A-D79C-4F8E-B818-89ACB68E1EFC</a:ObjectID>
<a:Name>id_departement</a:Name>
<a:Code>idDepartement</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {6873B036-8BD3-4CAE-8278-E0F35F8CF69C}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_DEPARTEMENT</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o128">
<a:ObjectID>253127DA-5702-4488-B469-698E930B2264</a:ObjectID>
<a:Name>nom_departement</a:Name>
<a:Code>nomDepartement</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {7725CFBC-CA1B-4BAA-B015-CD9A96CFAD82}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_DEPARTEMENT</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o129">
<a:ObjectID>2D8F940E-37BB-4C4E-AD8E-075FA73D98FC</a:ObjectID>
<a:Name>date_creation</a:Name>
<a:Code>dateCreation</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {C53DC75A-D950-4335-B392-0156508B3094}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>DATE_CREATION</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o130">
<a:ObjectID>AD32FC3D-57FA-40B2-BE41-7F1253DA63D9</a:ObjectID>
<a:Name>numero_texte_creation</a:Name>
<a:Code>numeroTexteCreation</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {160ADCFC-76C2-4AD7-812A-CB5372E605FF}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NUMERO_TEXTE_CREATION</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o131">
<a:ObjectID>362A9633-6E78-4880-A595-97AC73E0D711</a:ObjectID>
<a:Name>id_departement</a:Name>
<a:Code>ID_DEPARTEMENT</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E177E02A-5FE5-4455-B9FD-8E77CE3C66E1}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o127"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o131"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o83">
<a:ObjectID>34EFED1C-375B-4A44-8E67-E9AB560A0568</a:ObjectID>
<a:Name>Filiere</a:Name>
<a:Code>Filiere</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {5E33B9EA-B19F-4902-A628-5F86E01A2F76}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>FILIERE</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o132">
<a:ObjectID>23001C98-EA20-4394-B1BD-8AA5794C49AE</a:ObjectID>
<a:Name>id_filiere</a:Name>
<a:Code>idFiliere</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {AA132080-2206-4C21-95E3-B99B50E5DD30}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_FILIERE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o133">
<a:ObjectID>EBABDBD5-F8BA-4E36-9F37-01D6A5959FE1</a:ObjectID>
<a:Name>code_filliere</a:Name>
<a:Code>codeFilliere</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {1DEBBB27-719A-443C-9469-EF532F35817A}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>CODE_FILLIERE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o134">
<a:ObjectID>F76D9AF8-609B-4D3D-B3EB-AA66E92FA5F5</a:ObjectID>
<a:Name>nom_filiere</a:Name>
<a:Code>nomFiliere</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {9CD0C61C-1F9D-4A0C-A14E-455D74647867}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_FILIERE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o135">
<a:ObjectID>70D5260D-EBD4-4EC8-B76A-83EA1C61EB4B</a:ObjectID>
<a:Name>cycle</a:Name>
<a:Code>cycle</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {DFC151E8-FC34-43BB-840C-CA7288631AFA}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>CYCLE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o136">
<a:ObjectID>D1549109-6B7E-44A3-8E94-0420BACFA09B</a:ObjectID>
<a:Name>date_creation</a:Name>
<a:Code>dateCreation</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {364D7D09-42FE-466E-B2C0-8430D900CF0D}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>DATE_CREATION</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o137">
<a:ObjectID>5FD2AAF6-BD27-4D28-90A7-623CBD08077F</a:ObjectID>
<a:Name>id_filiere</a:Name>
<a:Code>ID_FILIERE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {3B40FA4B-E0DA-413C-BA3A-D0B08EDEC117}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o132"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o137"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o84">
<a:ObjectID>5E4DB56F-6A57-48EE-90B2-7A4193125275</a:ObjectID>
<a:Name>Classe</a:Name>
<a:Code>Classe</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {C4CFAE2E-2A4B-47C9-B307-D709A9430B6E}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>CLASSE</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o138">
<a:ObjectID>6A076542-028B-4BE0-BFD8-780E1117B6FB</a:ObjectID>
<a:Name>id_classe</a:Name>
<a:Code>idClasse</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {45392BC3-E1A8-4656-B514-2F9E9CF0156B}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_CLASSE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o139">
<a:ObjectID>FF6F8EC6-50E1-4C46-9354-00543424D3F5</a:ObjectID>
<a:Name>nom_classe</a:Name>
<a:Code>nomClasse</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {B094507C-2EEB-4C29-A912-5EC0875EAAF5}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_CLASSE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o140">
<a:ObjectID>01F51471-9387-42BD-8DA0-505FE02F4952</a:ObjectID>
<a:Name>niveau</a:Name>
<a:Code>niveau</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {3586B68A-7D55-43E4-B938-0E5219D27E08}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>NIVEAU</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o141">
<a:ObjectID>3E192639-E249-4521-A55C-BDB5331D173A</a:ObjectID>
<a:Name>type</a:Name>
<a:Code>type</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {91331660-7116-4DD1-8B27-FD5E81E89B1F}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TYPE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o142">
<a:ObjectID>2046C1D6-079B-4F77-8FD2-6B7A4CAC0BBD</a:ObjectID>
<a:Name>id_classe</a:Name>
<a:Code>ID_CLASSE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {3B817EBC-A218-4941-A0AB-86C481B60FCC}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o138"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o142"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o85">
<a:ObjectID>BED1A58D-D5CD-4602-929F-85235C376078</a:ObjectID>
<a:Name>Enseignant</a:Name>
<a:Code>Enseignant</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {A021CA09-480B-4650-A0F9-E391A65BC10B}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>ENSEIGNANT</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o143">
<a:ObjectID>234BDB45-BF17-4D57-BB00-ADE573156736</a:ObjectID>
<a:Name>id_enseignant</a:Name>
<a:Code>idEnseignant</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {6A2CEB81-CF51-4A30-B1C4-FC7A670DDD1D}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_ENSEIGNANT</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o144">
<a:ObjectID>956BA053-C417-4724-8320-7360A8427111</a:ObjectID>
<a:Name>nom_prenom</a:Name>
<a:Code>nomPrenom</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {1D913EBB-1294-4B6A-B4C4-9F4BB5A502A7}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_PRENOM</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o145">
<a:ObjectID>6A43022D-5F5C-4E0F-8E3F-47218AA9FDE4</a:ObjectID>
<a:Name>grade</a:Name>
<a:Code>grade</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {BF4845A4-9431-41C1-8FDC-6A923F05F723}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>GRADE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o146">
<a:ObjectID>AD2C4F90-ED15-4CDB-8121-7859B0BD15A2</a:ObjectID>
<a:Name>numero_cni</a:Name>
<a:Code>numeroCni</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {05BA5FBB-CBAA-45D9-94F6-9E9C9B97BB76}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NUMERO_CNI</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o147">
<a:ObjectID>FD4F18F1-95C4-41A2-A335-0FABD8ABF192</a:ObjectID>
<a:Name>specialite_recherche</a:Name>
<a:Code>specialiteRecherche</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {FCB7D262-5BEA-4BB5-A465-A97987671511}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>SPECIALITE_RECHERCHE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o148">
<a:ObjectID>37827594-B81F-41BD-BBC7-220694F5D1A7</a:ObjectID>
<a:Name>telephone</a:Name>
<a:Code>telephone</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {4455A2C0-B0FE-460D-B2D6-6C6E5BB78E49}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TELEPHONE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o149">
<a:ObjectID>137342D6-8F00-4DD2-A67A-4F1FA58C4D4F</a:ObjectID>
<a:Name>ville_residence</a:Name>
<a:Code>villeResidence</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F423F774-241F-42EB-8A38-02C03CA6A242}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>VILLE_RESIDENCE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o150">
<a:ObjectID>C273536C-4072-4876-9023-208C8FBC1288</a:ObjectID>
<a:Name>categorie</a:Name>
<a:Code>categorie</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {EDB24BDA-E089-47D5-9C73-DD688D90EFC6}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>CATEGORIE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o151">
<a:ObjectID>125F3A25-C25A-4F35-B6E8-0C579E566154</a:ObjectID>
<a:Name>sexe</a:Name>
<a:Code>sexe</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {EFF0D4E0-AC97-402E-9309-7F4C2DCFAC61}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>SEXE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o152">
<a:ObjectID>93B0EA1A-AD4A-4DC1-8999-7428D0472051</a:ObjectID>
<a:Name>id_enseignant</a:Name>
<a:Code>ID_ENSEIGNANT</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {86DC2BD2-0766-407F-8045-4117DD0738B4}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o143"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o152"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o89">
<a:ObjectID>55BFF5C3-6959-44DB-947A-B3B54E996313</a:ObjectID>
<a:Name>ElementConstitutif</a:Name>
<a:Code>ElementConstitutif</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {586A1D32-BD31-46E0-8F7E-D746F726E3EA}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>ELEMENTCONSTITUTIF</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o153">
<a:ObjectID>947BF3EE-A22C-44B7-AE90-35EFE21391A7</a:ObjectID>
<a:Name>id_element_constitutif</a:Name>
<a:Code>idElementConstitutif</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {B91AF9BF-465C-4226-8086-055C14B678E1}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_ELEMENT_CONSTITUTIF</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o154">
<a:ObjectID>F615D7D6-D2CB-4E6D-956B-D7928C7A9E84</a:ObjectID>
<a:Name>code_ec</a:Name>
<a:Code>codeEc</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {02691326-36B2-4F83-8353-462C29692EC0}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>CODE_EC</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o155">
<a:ObjectID>B75563CF-2508-4CB5-AE9F-2CBCB5714955</a:ObjectID>
<a:Name>nom_ec</a:Name>
<a:Code>nomEc</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {4B781B9E-5FAD-4294-A5F3-FB66D88F3058}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_EC</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o156">
<a:ObjectID>57542217-8081-46C3-B4B7-771B571D5A8D</a:ObjectID>
<a:Name>code_ec</a:Name>
<a:Code>CODE_EC</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {24DC4551-97F4-4576-9D86-1793B972E656}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o153"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o156"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o87">
<a:ObjectID>68EE56BE-7084-443A-96E9-A43A66672C76</a:ObjectID>
<a:Name>ElementGrille</a:Name>
<a:Code>ElementGrille</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {5E3A29DB-A627-41EE-8CFA-DEC14F5D1194}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>ELEMENTGRILLE</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o157">
<a:ObjectID>02EDBCE0-810B-40AB-8B4C-8C4442C49988</a:ObjectID>
<a:Name>id_element_grille</a:Name>
<a:Code>idElementGrille</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E4A84C01-2D37-4D53-901D-C3812AE85B04}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_ELEMENT_GRILLE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o158">
<a:ObjectID>7564DAE5-983F-4FCD-A9A7-D63D1C998265</a:ObjectID>
<a:Name>pourcentageTP</a:Name>
<a:Code>pourcentageTP</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {884FC3C5-9CE0-4C4D-9B86-204F664F9E4C}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>POURCENTAGETP</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o159">
<a:ObjectID>CBA04B23-7AC9-4551-9492-2DC75ECA948D</a:ObjectID>
<a:Name>pourcentageProjet</a:Name>
<a:Code>pourcentageProjet</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {66632B37-FC34-4921-B978-10203C096900}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>POURCENTAGEPROJET</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o160">
<a:ObjectID>6FD94F80-34F3-48A7-A690-AFC6FB6212FA</a:ObjectID>
<a:Name>pourcentageCC</a:Name>
<a:Code>pourcentageCC</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {60D1FE1D-670E-49F3-8630-B7D8F0F19A6E}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>POURCENTAGECC</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o161">
<a:ObjectID>977D79BB-A8EF-44C5-B07A-C682FDA44E87</a:ObjectID>
<a:Name>pourcentageExamen</a:Name>
<a:Code>pourcentageExamen</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {3C115279-E73A-4559-9D14-BE86A9CC7170}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>POURCENTAGEEXAMEN</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o162">
<a:ObjectID>D18AEF69-EB22-44E0-B526-CA5575135FB7</a:ObjectID>
<a:Name>credit</a:Name>
<a:Code>credit</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {2BE97EB9-F5D3-40AD-A456-35A94367E4E1}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>CREDIT</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o163">
<a:ObjectID>BC206BFE-499F-4B1F-9A13-4B81268A21EC</a:ObjectID>
<a:Name>semestre</a:Name>
<a:Code>semestre</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {09864809-37A8-4EB6-8E9F-C74FF08AF246}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>SEMESTRE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o164">
<a:ObjectID>0CA0F756-31C6-4D00-B1F4-43E948156AAF</a:ObjectID>
<a:Name>annee_academique</a:Name>
<a:Code>anneeAcademique</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {35E561C9-11FF-4B50-9EE9-A63FBF493787}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ANNEE_ACADEMIQUE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o165">
<a:ObjectID>59B5BE47-0F60-466B-A534-8074EF124B12</a:ObjectID>
<a:Name>id_element_grille</a:Name>
<a:Code>ID_ELEMENT_GRILLE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {460C89F1-012B-48C9-BA4A-7F67A7E0B78E}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o157"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o165"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o91">
<a:ObjectID>EA8CDCA2-D791-485B-9A3A-944F5B1BADBE</a:ObjectID>
<a:Name>Profil</a:Name>
<a:Code>Profil</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {08A2D525-A9FD-47E7-A0FD-5397BB649E01}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>PROFIL</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o166">
<a:ObjectID>B0B4B9D6-FBCE-49B6-9F27-7440A131D49E</a:ObjectID>
<a:Name>id_profil</a:Name>
<a:Code>idProfil</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D095A097-A773-4676-BDF3-7D71C8A05578}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_PROFIL</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o167">
<a:ObjectID>D8ABA131-06FC-44A3-A7FA-116507E82202</a:ObjectID>
<a:Name>nom_profil</a:Name>
<a:Code>nomProfil</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {17E07D7C-4684-49DB-9EB1-ADCFEAC57CC5}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_PROFIL</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o168">
<a:ObjectID>C8C33D0C-9BA4-4859-9B2B-60F268756E10</a:ObjectID>
<a:Name>type_profil</a:Name>
<a:Code>typeProfil</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {7B6C0981-E737-4858-A767-1096C95BD5D6}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TYPE_PROFIL</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o169">
<a:ObjectID>70C24D0B-E0A0-4A71-9A0C-F7E2365FCE93</a:ObjectID>
<a:Name>id_profil</a:Name>
<a:Code>ID_PROFIL</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {06D286F7-5C79-4A5C-BE38-CBB069F9C770}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o166"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o169"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o93">
<a:ObjectID>7CDDC470-B2CE-4476-AFA6-9AFEE0749CB4</a:ObjectID>
<a:Name>Privilege</a:Name>
<a:Code>Privilege</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E9856C0F-6935-4ADF-A652-A02C7E82BB77}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<a:PersistentCode>PRIVILEGE</a:PersistentCode>
<c:Attributes>
<o:Attribute Id="o170">
<a:ObjectID>85A54208-B68F-44BA-8116-F3486AED4698</a:ObjectID>
<a:Name>id_privilege</a:Name>
<a:Code>idPrivilege</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D59EDFA5-87DE-475F-90E2-378B8AB87F9C}
DAT 1584626688</a:History>
<a:DataType>int</a:DataType>
<a:PersistentCode>ID_PRIVILEGE</a:PersistentCode>
<a:Multiplicity>1..1</a:Multiplicity>
</o:Attribute>
<o:Attribute Id="o171">
<a:ObjectID>61C6770F-0409-4413-B5FC-3A5564904D9E</a:ObjectID>
<a:Name>nom_privilege</a:Name>
<a:Code>nomPrivilege</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D56B92FD-C151-444A-A992-504849C3F769}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>NOM_PRIVILEGE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o172">
<a:ObjectID>56DB90EC-FA02-4ACD-9502-8C65CFFD03DA</a:ObjectID>
<a:Name>type_privilege</a:Name>
<a:Code>typePrivilege</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F8142BCC-11BA-4680-9E24-CDBED3422047}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TYPE_PRIVILEGE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o173">
<a:ObjectID>75C758AA-A4A1-4C7F-B594-070B86624497</a:ObjectID>
<a:Name>description</a:Name>
<a:Code>description</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {49A67835-CD25-4783-94EC-23C5AB415ABC}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>DESCRIPTION</a:PersistentCode>
</o:Attribute>
</c:Attributes>
<c:Identifiers>
<o:Identifier Id="o174">
<a:ObjectID>78E003C7-4B5D-4397-AABD-FF5C7B546400</a:ObjectID>
<a:Name>id_privilege</a:Name>
<a:Code>ID_PRIVILEGE</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F1353C43-FDFB-437E-9086-CE4C6352378A}
DAT 1584626688</a:History>
<c:Identifier.Attributes>
<o:Attribute Ref="o170"/>
</c:Identifier.Attributes>
</o:Identifier>
</c:Identifiers>
<c:PrimaryIdentifier>
<o:Identifier Ref="o174"/>
</c:PrimaryIdentifier>
</o:Class>
<o:Class Id="o86">
<a:ObjectID>B8C35B6E-CE10-4F66-8D74-310FBBC7424D</a:ObjectID>
<a:Name>Enseigner</a:Name>
<a:Code>Enseigner</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {900FC89D-65DA-4FCE-AB50-D37562680A91}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o175">
<a:ObjectID>C27F0575-2637-4266-90D7-BA7FB52ED20B</a:ObjectID>
<a:Name>role</a:Name>
<a:Code>role</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {46CC1017-EBF1-48AE-9A20-ECFCC51C5FEE}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ROLE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o88">
<a:ObjectID>B57DFF9D-175D-461A-8B1A-C4EFAA713C52</a:ObjectID>
<a:Name>Composition</a:Name>
<a:Code>Composition</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F0E76E6B-69D2-42CB-9CD1-F0A31F6CF541}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o176">
<a:ObjectID>4C378A5F-014D-4140-8625-C17EBA904633</a:ObjectID>
<a:Name>note</a:Name>
<a:Code>note</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {740D1655-98C0-42F0-B74A-FF19612FF765}
DAT 1584626688</a:History>
<a:DataType>double</a:DataType>
<a:PersistentCode>NOTE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o177">
<a:ObjectID>A269B87B-DA25-444C-BC0B-139AFF8D622C</a:ObjectID>
<a:Name>type_composition</a:Name>
<a:Code>typeComposition</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {7E3B7769-660D-4529-B865-09BAA5C92B34}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>TYPE_COMPOSITION</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o178">
<a:ObjectID>C6ECA435-3E3F-473F-BDC9-5BE77C5CE12E</a:ObjectID>
<a:Name>date_composition</a:Name>
<a:Code>dateComposition</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {0B919C42-C49E-471E-AB94-073CDF12B05E}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>DATE_COMPOSITION</a:PersistentCode>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o90">
<a:ObjectID>BAE59250-583D-4B4B-9AD8-D96C68C8485E</a:ObjectID>
<a:Name>Inscrire</a:Name>
<a:Code>Inscrire</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {66B7952C-CC11-4BBA-BEB2-1F90F2EDC853}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o179">
<a:ObjectID>C83472EA-05BD-435E-9357-815B7140E66B</a:ObjectID>
<a:Name>annee_academique</a:Name>
<a:Code>anneeAcademique</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {451206C5-0975-4901-BDB9-455B0AEB42A4}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ANNEE_ACADEMIQUE</a:PersistentCode>
</o:Attribute>
</c:Attributes>
</o:Class>
<o:Class Id="o92">
<a:ObjectID>CBF6A26C-1C12-430C-AB7E-FDBBD65EB884</a:ObjectID>
<a:Name>Appartient</a:Name>
<a:Code>Appartient</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {7405E234-76A0-406B-A755-D141D371CF7F}
DAT 1584626688</a:History>
<a:UseParentNamespace>0</a:UseParentNamespace>
<c:Attributes>
<o:Attribute Id="o180">
<a:ObjectID>9003DCF8-C611-4CB5-9656-9FA9572D4E55</a:ObjectID>
<a:Name>role</a:Name>
<a:Code>role</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {732F2CE9-4151-48A6-BC79-8494B4A3F51E}
DAT 1584626688</a:History>
<a:DataType>String</a:DataType>
<a:PersistentCode>ROLE</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o181">
<a:ObjectID>B28D3335-5C6C-4C48-B217-D0D62743BDCC</a:ObjectID>
<a:Name>debut_fonction</a:Name>
<a:Code>debutFonction</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {68A71188-3EB8-4BAA-902A-B00E270F898A}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>DEBUT_FONCTION</a:PersistentCode>
</o:Attribute>
<o:Attribute Id="o182">
<a:ObjectID>07F45DB4-6D0E-4F88-891F-C7B42FADDDAC</a:ObjectID>
<a:Name>fin_fonction</a:Name>
<a:Code>finFonction</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {04052CC6-51C8-4488-962A-51E64F5E6D10}
DAT 1584626688</a:History>
<a:DataType>Date</a:DataType>
<a:PersistentCode>FIN_FONCTION</a:PersistentCode>
</o:Attribute>
</c:Attributes>
</o:Class>
</c:Classes>
<c:Associations>
<o:Association Id="o9">
<a:ObjectID>6778AC0A-612B-4856-89E4-419FC303ABC1</a:ObjectID>
<a:Name>Enseigner</a:Name>
<a:Code>enseigner</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871475</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {900FC89D-65DA-4FCE-AB50-D37562680A91}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o85"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o87"/>
</c:Object2>
</o:Association>
<o:Association Id="o16">
<a:ObjectID>8AF6E414-6D07-45BF-9B90-0BCD0EE8E0C0</a:ObjectID>
<a:Name>Inclu</a:Name>
<a:Code>inclu</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {1B1C5113-7733-447D-B4C4-8287F74E4F48}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o83"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o84"/>
</c:Object2>
</o:Association>
<o:Association Id="o19">
<a:ObjectID>3F2C9550-3B94-4513-8314-FE265FB2610A</a:ObjectID>
<a:Name>Utiliser</a:Name>
<a:Code>utiliser</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {81A97AF8-615A-43A6-9056-62B330D948BF}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o84"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o81"/>
</c:Object2>
</o:Association>
<o:Association Id="o22">
<a:ObjectID>AF6AEBA6-0ED2-4E48-8235-8ED07E81DD2A</a:ObjectID>
<a:Name>Avoir</a:Name>
<a:Code>avoir</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {2BA6099A-863F-4960-9BF0-46245363A70A}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o82"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o83"/>
</c:Object2>
</o:Association>
<o:Association Id="o26">
<a:ObjectID>2AA0E6BE-0888-4246-BC02-F33E1B7DF07D</a:ObjectID>
<a:Name>Coder</a:Name>
<a:Code>coder</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {BF91D929-901B-4713-B95B-788D664202DC}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o77"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o79"/>
</c:Object2>
</o:Association>
<o:Association Id="o30">
<a:ObjectID>4C461B23-F342-437E-B524-B49A27770EB9</a:ObjectID>
<a:Name>Mener</a:Name>
<a:Code>mener</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {35952465-3362-4336-9C06-000F8B3624A4}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o76"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o78"/>
</c:Object2>
</o:Association>
<o:Association Id="o32">
<a:ObjectID>71E337B1-F9E6-47CE-A919-9221C1D577FF</a:ObjectID>
<a:Name>Composition</a:Name>
<a:Code>composition</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871475</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {F0E76E6B-69D2-42CB-9CD1-F0A31F6CF541}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o77"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o87"/>
</c:Object2>
</o:Association>
<o:Association Id="o38">
<a:ObjectID>8186A762-C6B8-4310-94CB-6D1874B200B2</a:ObjectID>
<a:Name>Contenir</a:Name>
<a:Code>contenir</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {DA1B8567-147D-400A-B998-0CA1A75EB5E4}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o80"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o87"/>
</c:Object2>
</o:Association>
<o:Association Id="o41">
<a:ObjectID>7C15A1C0-850A-49E1-8B44-226CF5BCF8DD</a:ObjectID>
<a:Name>Constituer</a:Name>
<a:Code>constituer</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E6B46F1C-6EB0-4446-B1D3-63DF2F63DC72}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o89"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o87"/>
</c:Object2>
</o:Association>
<o:Association Id="o43">
<a:ObjectID>CF8754E1-384B-4F98-9844-9E54E77E59ED</a:ObjectID>
<a:Name>Appartenir</a:Name>
<a:Code>appartenir</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D9981504-C2E2-4AE3-A38E-BE9BF4BEAA3C}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o81"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o87"/>
</c:Object2>
</o:Association>
<o:Association Id="o45">
<a:ObjectID>82E0843C-E1F0-4FAE-AA12-87CC2EDEE5A0</a:ObjectID>
<a:Name>Anonymer</a:Name>
<a:Code>anonymer</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E8DB7F73-6F39-429D-AE43-101024B1BF4F}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o80"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o79"/>
</c:Object2>
</o:Association>
<o:Association Id="o47">
<a:ObjectID>7D3CE685-B6F3-4DB9-BD6E-578B61A956A6</a:ObjectID>
<a:Name>Inscrire</a:Name>
<a:Code>inscrire</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871475</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {66B7952C-CC11-4BBA-BEB2-1F90F2EDC853}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o77"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o84"/>
</c:Object2>
</o:Association>
<o:Association Id="o53">
<a:ObjectID>E16500EC-9029-4C6F-9EE6-25953BBF51F0</a:ObjectID>
<a:Name>Utilise</a:Name>
<a:Code>utilise</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {D5D943D1-8D8D-4DC1-A56E-613BCCCF7075}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o76"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o91"/>
</c:Object2>
</o:Association>
<o:Association Id="o55">
<a:ObjectID>CA41DBF5-A905-4726-A68D-A826C551BCFB</a:ObjectID>
<a:Name>Appartient</a:Name>
<a:Code>appartient</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584871476</a:ModificationDate>
<a:Modifier>#SoftGraphic</a:Modifier>
<a:History>ORG {7405E234-76A0-406B-A755-D141D371CF7F}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o85"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o82"/>
</c:Object2>
</o:Association>
<o:Association Id="o61">
<a:ObjectID>BEDF951F-9D1B-407B-A61B-E100B166337B</a:ObjectID>
<a:Name>Privilégier</a:Name>
<a:Code>privilegier</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {C1E31983-211F-4504-80B3-DD62F90DFECB}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>1..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o91"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o93"/>
</c:Object2>
</o:Association>
<o:Association Id="o63">
<a:ObjectID>9885593F-A9E8-445D-83EC-520E32AF8843</a:ObjectID>
<a:Name>Reinscrire</a:Name>
<a:Code>reinscrire</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {78656103-D056-497C-8290-601D024013A6}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o77"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o80"/>
</c:Object2>
</o:Association>
<o:Association Id="o65">
<a:ObjectID>EC829063-C5A6-467B-A1A4-F512EDABFEC6</a:ObjectID>
<a:Name>Desinscrire</a:Name>
<a:Code>desinscrire</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {F8194D8B-401C-4F9A-80B6-8180506720FD}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o77"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o80"/>
</c:Object2>
</o:Association>
<o:Association Id="o67">
<a:ObjectID>C7E29BE5-AEE8-467B-BB38-67C01FEEC120</a:ObjectID>
<a:Name>Responsable</a:Name>
<a:Code>responsable</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {0E7FEAA6-2C4D-4F16-948F-145FAA12252B}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..1</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<a:RoleBNavigability>0</a:RoleBNavigability>
<c:Object1>
<o:Class Ref="o85"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o80"/>
</c:Object2>
</o:Association>
<o:Association Id="o69">
<a:ObjectID>F5CAA5D8-3AAE-4681-86BA-DB72DB7CA8E9</a:ObjectID>
<a:Name>Composer</a:Name>
<a:Code>composer</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {5721881C-5A3F-4BAB-9477-283456866129}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o84"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o84"/>
</c:Object2>
</o:Association>
<o:Association Id="o71">
<a:ObjectID>96EBC53D-4DC1-49E3-9666-188C83407BDD</a:ObjectID>
<a:Name>Configurer</a:Name>
<a:Code>configurer</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {E6A61249-5614-4AC0-A4DB-CF7B9D5AE2C0}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..*</a:RoleAMultiplicity>
<a:RoleBMultiplicity>1..*</a:RoleBMultiplicity>
<a:RoleANavigability>1</a:RoleANavigability>
<c:Object1>
<o:Class Ref="o87"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o84"/>
</c:Object2>
</o:Association>
<o:Association Id="o73">
<a:ObjectID>5DC5CF80-24D4-4C3B-9846-F75EC45DB65E</a:ObjectID>
<a:Name>etre</a:Name>
<a:Code>etre</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {33DC8C6E-F323-40D4-B29F-7E3010E5C4D9}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o76"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o77"/>
</c:Object2>
</o:Association>
<o:Association Id="o75">
<a:ObjectID>94FC47A1-611D-4595-A39F-1DFA13FC4871</a:ObjectID>
<a:Name>etre</a:Name>
<a:Code>etre</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:History>ORG {6E877C60-F6A5-4FD6-BD10-483091DBAE58}
DAT 1584626688</a:History>
<a:RoleAMultiplicity>0..1</a:RoleAMultiplicity>
<a:RoleBMultiplicity>0..*</a:RoleBMultiplicity>
<c:Object1>
<o:Class Ref="o76"/>
</c:Object1>
<c:Object2>
<o:Class Ref="o85"/>
</c:Object2>
</o:Association>
</c:Associations>
<c:AssociationClassLinks>
<o:AssociationClassLink Id="o12">
<a:ObjectID>8365B89C-BEAB-4C42-9228-8377F185C8F0</a:ObjectID>
<a:CreationDate>0</a:CreationDate>
<a:Creator/>
<a:ModificationDate>0</a:ModificationDate>
<a:Modifier/>
<c:Object1>
<o:Class Ref="o86"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o9"/>
</c:Object2>
</o:AssociationClassLink>
<o:AssociationClassLink Id="o35">
<a:ObjectID>23203BE5-326C-4F92-8C64-FE86695B12D5</a:ObjectID>
<a:CreationDate>0</a:CreationDate>
<a:Creator/>
<a:ModificationDate>0</a:ModificationDate>
<a:Modifier/>
<c:Object1>
<o:Class Ref="o88"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o32"/>
</c:Object2>
</o:AssociationClassLink>
<o:AssociationClassLink Id="o50">
<a:ObjectID>A42E2EBB-0CD8-4EB4-92EC-941D56F67000</a:ObjectID>
<a:CreationDate>0</a:CreationDate>
<a:Creator/>
<a:ModificationDate>0</a:ModificationDate>
<a:Modifier/>
<c:Object1>
<o:Class Ref="o90"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o47"/>
</c:Object2>
</o:AssociationClassLink>
<o:AssociationClassLink Id="o58">
<a:ObjectID>1772984C-9C74-40DD-8690-FAD1A40B66D5</a:ObjectID>
<a:CreationDate>0</a:CreationDate>
<a:Creator/>
<a:ModificationDate>0</a:ModificationDate>
<a:Modifier/>
<c:Object1>
<o:Class Ref="o92"/>
</c:Object1>
<c:Object2>
<o:Association Ref="o55"/>
</c:Object2>
</o:AssociationClassLink>
</c:AssociationClassLinks>
<c:TargetModels>
<o:TargetModel Id="o183">
<a:ObjectID>C3988F8C-065D-4BE5-87B6-5234737A8D3D</a:ObjectID>
<a:Name>Analyse</a:Name>
<a:Code>Analysis</a:Code>
<a:CreationDate>1584626674</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626674</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:TargetModelURL>file:///%_OBJLANG%/analysis.xol</a:TargetModelURL>
<a:TargetModelID>87317290-AF03-469F-BC1E-99593F18A4AB</a:TargetModelID>
<a:TargetModelClassID>1811206C-1A4B-11D1-83D9-444553540000</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o4"/>
</c:SessionShortcuts>
</o:TargetModel>
<o:TargetModel Id="o184">
<a:ObjectID>A9CEE571-21A6-4812-AF33-B709D1B5F9FD</a:ObjectID>
<a:Name>Données conceptuelles_1</a:Name>
<a:Code>DONNEES_CONCEPTUELLES_1</a:Code>
<a:CreationDate>1584626688</a:CreationDate>
<a:Creator>hp</a:Creator>
<a:ModificationDate>1584626688</a:ModificationDate>
<a:Modifier>hp</a:Modifier>
<a:TargetModelURL>file:///D|/ENS BERTOUA/Mémoire/Modelisation/SAGGE_NOTES 3_0/Données conceptuelles_1.mcd</a:TargetModelURL>
<a:TargetModelID>5EF6F072-FC6E-41E7-AAC2-2B77AC4ECDFA</a:TargetModelID>
<a:TargetModelClassID>1E597170-9350-11D1-AB3C-0020AF71E433</a:TargetModelClassID>
<c:SessionShortcuts>
<o:Shortcut Ref="o3"/>
</c:SessionShortcuts>
</o:TargetModel>
</c:TargetModels>
</o:Model>
</c:Children>
</o:RootObject>

</Model>